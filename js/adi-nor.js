
/********************************************************************************************************************************************************************************************


                    AKERSELVA DIGITAL  - an audio walking guide for Industrimuseum.no, a subsidiary of Teknisk Museum. This guide is to be used for walking along Akerselva, the main 
                    river in Oslo. The application uses geolocation, and automatically plays audio tracks relevant to points of interest when the user is in proximity. The audio retells
                    stories related to old industry or commercial places of interest.  [2013 - developed by BOUVET ASA for Teknisk Museum.

*********************************************************************************************************************************************************************************************/


Number.prototype.toRad = function () {
    "use strict";
    return this * Math.PI / 180;
}

// The Points of Interest - pois
// in the next version, these will be read from server,
var foundationData = [{'description': 'Akerselva er verdt en vandring. Enten om du tar en sving nedom eller setter av noen timers frisk gange tvers gjennom byen langs hele elvas drøyt 8 kilometer, så får du deg en naturopplevelse, kanskje en aha-opplevelse, og du får kjenne Olso rett på hovedpulsåren. En vandring langs elva gir mulighet for å lese og oppleve vår egen historie. I Europeisk sammenheng er Akerselvas industrihistoriske landskap helt unikt og svært verdifullt av mange grunner. Akerselva er den eneste typiske industi-elv som renner tvers gjennom hovedstad med start og slutt innenfor bygrensene. Elva er helt spesiell i representasjon av ulike industrigrener og tidsepoker. Her er 1700 tallets industribygg av tre, her er 1800-tallets typiske fabrikker av rød murstein med jernsprossede vinduer, shed-tak og høye murte fabrikkpiper, og her er moderne industribygg fra industrialismens senere faser og dagens eksisterende industri. Her er bevarte miljøer av arbeiderboliger fra ulike epoker. Her er mange spor av teknisk infrastruktur med demninger, broer, rørgater og dammer. Gjenbruk av industriområdene til nye formål er typiske og godt egnet til å studere avindustrialiseringsprosesser og gjenbruk av storbyers industriområder.\r\nElva er tilrettelagt med turstier og parkanlegg, museer og andre institusjoner som bevarer og formidler industrihistorien, og kafeer og kulturtilbud langs hele elva fra Kjelsås til Bjørvika. Vi ønsker at alle som bor i Oslo og besøker Oslo skal få oppleve Akerselvas unike kulturminnelandskap i det som er et sammenhengende utemuseum gjennom byen. God tur!\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-081.jpg', 'text': '', 'alttext': 'Bjøsen valsemøle sett fra elvebredden på nedsiden av fossen', 'width': 140, 'height': 242}, 'title': 'Akerselva - et museum gjennom byen', 'url': 'http://www.industrimuseum.no/00akerselvadigitalt_intro', 'coords': '59.92383, 10.75374', 'audio': ['http://www.industrimuseum.no/filearchive/002_Akerselva_-_Et_museum_gjennom_byen.mp3', 'http://www.industrimuseum.no/filearchive/001_Akerselva_-_Et_museum_gjennom_byen__Duc_.mp3']}, {'description': 'Vilhelm Dybwad (1863-1950) var advokat og forfatter og visedikter, og en markant figur i Oslos teater- og revyliv. Blant hans svært mange vise- og revytekster er det særlig Akerselva-sangen fra 1906 som blir husket og knyttet til Oslo. Mange oppfatter akkurat denne sangen som selve "Oslo-hymnen" - og det til tross for at teksten er en ironisk kommentar til den skitne Akerselva! Teksten satt til en østerriksk melodi ble først fremført av Centralteaterets stjerne og mangeårige teatersjef Harald Otto i teaterets underholdningsforestilling Verdens Undergang. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_centralteatret_norskfolkemuseumNF10044_051.jpg', 'text': 'Centralteatret med fasaden slik den så ut da Akerselvasangen ble fremført her i 1906, og før dagens funkisfasader ble bygd på 1930-tallet.', 'alttext': 'Centralteatret (Ingressbilde)', 'width': 187, 'height': 242}, 'title': 'Akerselva, du gamle og grå!', 'url': 'http://www.industrimuseum.no/akerselvasangen_dybwad', 'coords': '59.90742574114085, 10.755271911621093', 'audio': ['http://www.industrimuseum.no/filearchive/akerselvasangen_wicklund2008-1..mp3', 'http://www.industrimuseum.no/filearchive/akerselvasangen_fanfare_jollykramer.mp3']},  {'description': 'Akerselvas ferd starter ved Maridalsoset. Kjelsås på østsiden og Brekke i vest markerer Akerselvas start og overgangen mellom skog og by. Ved Brekkedammen var det sagbruk og møller i mange hundre år. Eventyrforteller Asbjørnsen beskrev området i "Kvernsagn". Med industrialiseringen rundt 1850 kom også verkstedindustri til Kjelsås med Kjelsås Brug. Hundre år senere fant moderne industri som Tandberg, Ring og Hjemmets trykkeri veien til ledige industritomter på Brekke. Norsk Teknisk Museum flyttet hit i 1986. I dag er det fortsatt noen industri- og kontorbygg her, men mange er omgjort til boliger i det som er et av de mest populære boområdene i Oslo. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_forjul2011_0445_edited-1.jpg', 'text': 'Kjelsås i desember: Brekkedammen med Kjelsås Bruk og Teknisk museum i bakgrunnen', 'alttext': 'Kjelsås i desember: Brekkedammen med Kjelsås Bruk og Teknisk museum i bakgrunnen (Ingressbilde)', 'width': 237, 'height': 178}, 'title': 'Kjelsås - ved Akerselvas start og Teknisk museum', 'url': 'http://www.industrimuseum.no/01kjelsas_artikkel', 'coords': '59.96634800655828, 10.781965255737304', 'audio': ['http://www.industrimuseum.no/filearchive/003_Kjels_s.mp3', 'http://www.industrimuseum.no/filearchive/004_Kjels_s__Duc_.mp3']}, {'description': 'Fra den nye kjørebrua over Maridalsoset kan vi speide over Maridalsvannets 3,7 kvadratkilometer - 149 meter over havet. Under brua går demningen som kommunen bygde i 1853 for å heve vannspeilet med en hel meter. Dette skulle sikre jevnere vanntilførsel for den nye industrien og byens voksende befolkning. Siden har demningen blitt modernisert flere ganger, og brukes fortsatt til regulering og for å hindre flom i elva. På nedsiden av brua ser vi de steinsatte breddene som er formet slik for å lede tømmer trygt gjennom svingen på vei ned til Brekkedammen, der det var sagbruk helt til 1960-tallet. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20126.jpg', 'text': '', 'alttext': 'Maridalsvannet fra brua over Oset.', 'width': 237, 'height': 178}, 'title': 'Maridalsoset - byens kilde', 'url': 'http://www.industrimuseum.no/04maridalsoset', 'coords': '59.96937647722573, 10.78782320022583', 'audio': ['http://www.industrimuseum.no/filearchive/005_Maridalsoset_-_byens_kilde__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/006_Maridalsoset_-_byens_kilde.mp3']}, {'description': 'I 1855 etablerte kjøpmannen Johan Henrik Schmelck fabrikken Kjelsås Bruk for produksjon av spiker og ståltråd. I samme bransje var Gjøvik-firmaet O.Mustad og Sønn, som ble verdensledende på fiskekroker. Mustad produserte også alt fra spiker til margarin, blant annet på Lilleaker vest i Oslo. I 1884 kjøpte de Kjelsås Bruk og flyttet produksjonen av hesteskospiker hit. Bruket ble utvidet med ståltrådtrekkeri og et lite valseverk. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-030.jpg', 'text': '', 'alttext': 'Kjelsås Bruk - Mustad ved Brekkedamen / Teknisk museum', 'width': 237, 'height': 178}, 'title': 'O.Mustad & Sønn (Kjelsås Bruk)', 'url': 'http://www.industrimuseum.no/bedrifter/o_mustad_soenn%28kjelsaasbruk%29', 'coords': '59.966670045070494, 10.780965886755455', 'audio': ['http://www.industrimuseum.no/filearchive/007_Kjels_s_Bruk_-_Mustad__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/008_Kjels_s_Bruk_-_Mustad.mp3']}, {'description': 'Brekkedammen på Kjelsås ligger øverst i Akerselva. Her ble elva brukt som transportåre for tømmer fra Nordmarka og som kraftkilde for kverner og sager, trolig alt før 1600-tallet. Brekkesaga ble startet ca. 1740 av Nordmarkgodsets nye eier Christian Anker. Hans sønn Peder Anker og etterkommere i Wedel Jarlsberg og Løvenskiold-slektene holdt sagbrukstradisjonen i hevd ved Akerselva. Tømmer fra Nordmarka ble fløtet ned til Maridalsvannet og derfra ut i Akerselva. Sagdammen var dekket av tømmer om våren og sommeren. Det ble skåret både grove bjelker og fine planker. Tømmeret ble trukket fra vannet og opp til saga ved hjelp av en kjerrat, som er en slags heis for tømmerstokker. Ferdigskåret virke lå til tørk i store stabler rundt hele dammen.\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-032.jpg', 'text': 'Brekkedammen ned mot demningen fra 1890. Skråningen med tr\xe6rne i høyre kant er bakken hvor kjerraten lå.', 'alttext': 'Brekekdammen, skrningen ned mot dammen hvor kjerraten til Brekkesaga lå.', 'width': 237, 'height': 178}, 'title': 'Brekke Bruk - det siste sagbruket ved elva', 'url': 'http://www.industrimuseum.no/06brekkebruk_tekst', 'coords': ' 59.96711051993569, 10.77707290649414', 'audio': ['http://www.industrimuseum.no/filearchive/009_Brekkesaga.mp3', 'http://www.industrimuseum.no/filearchive/010_Brekkesaga__Duc_.mp3']}, {'description': 'Vebjørn Tandberg fra Bodø startet radiofabrikk i Oslo samme år som NRK startet, og "alle" skulle ha radio. Tandbergs Radiofabrikk ble landets største radio- og TV-fabrikk og et lokomotiv i norsk elektronikkindustri. Selskapet gikk konkurs i 1978, men deler av virksomheten levde videre.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_TANKJEL.JPG', 'text': 'Tandbergs Radiofabrikk på Kjelsås (Foto: Scan fra brosjyre)', 'alttext': 'tandberg_kjelsasfabrikk (Ingressbilde)', 'width': 238, 'height': 161}, 'title': 'Tandbergs Radiofabrikk', 'url': 'http://www.industrimuseum.no/bedrifter/tandbergradiofabrikk', 'coords': '59.9685555, 10.7726246 ', 'audio': ['http://www.industrimuseum.no/filearchive/011_Tandberg_radiofabrikk__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/012_Tandberg_radiofabrikk.mp3']}, {'description': 'I dag er Akerselva Miljøpark en flott vandring fra Grønland til Kjelsås. Slik har det ikke alltid vært. Industriområdene langs Akerselva sperret lange strekk av elva inne bak gjerder og under fabrikkbygninger, og satte spor med forurensing og utslipp. Tanken om å rehabilitere den forurensede Akerselva og opparbeide parker begynte på 1800-tallet. I 1915 ble de første kommunale palner lagt om å gjenvinne Akerselva som en grønn lunge gjennom byen – ”fra rynke til smil i byens ansikt”. Dette arbeidet skulle ta flere år, og begynte mer med spredte parker enn tanke på å se hele elveløpet samlet. Til det var eiendomsforholdene for komplisert og industrivirksomheten ved elva for viktig. Kommunen kjøpte gradvis ledige tomter og anla parker, først i 1915 nedenfor Ankerbrua. På 1920- og 30-tallet ble flere store tomter etter nedlagte teglverk ved elva som Schultzehaugen, Øvre Foss, Myrens og Bjølsen. Her ble industriområdene opparbeidet til parker fra 1930-årene. Gradvis kom andre områder etter, også i regi av private eiere av områder og bygg ved elva som så verdien i å utvikle eiendommer med historisk sus og nærhet til den stadig mer innbydende elva.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_DSC_0327.JPG', 'text': '', 'alttext': 'Akerselva i dag (Ingressbilde)', 'width': 238, 'height': 159}, 'title': 'Akerselva - fra rynke til smil', 'url': 'http://www.industrimuseum.no/akerselva-fra-rynke-til-smil', 'coords': '59.91912123705501, 10.756403803825378', 'audio': ['http://www.industrimuseum.no/filearchive/134_Akerselva_fra_rynke_til_smileb_nd__Duc_.mp3']},  {'description': 'En av landets første og største tekstilfabrikker ble anlagt ved et av Akerselvas øverst fall i 1845 av Adam Hiorth. Industrieventyret varte i drøyt 100 år, men bygningene er blant de best bevarte industrianleggene fra den tidligste industrialiseringen i Norge.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_nydalencomp-naa-C2433.jpg', 'text': '', 'alttext': 'nydalen3', 'width': 238, 'height': 233}, 'title': 'Nydalens Compagnie', 'url': 'http://www.industrimuseum.no/bedrifter/nydalenscompagnie', 'coords': '59.955778422295815, 10.765410666903662', 'audio': ['http://www.industrimuseum.no/filearchive/015_Nydalens_Compagnie__Duc_.mp3']}, {'description': 'Nydalen Compagnie og Christiania Spigerverk skapte stor befolkningsvekst i Nydalen. Lange arbeidsdager og vanskelig transport gjorde det nødvendig å bo nær arbeidsplassen. Det ble bygd lave trehus øverst i Sandakerveien og Gjerdrums vei, og i Maridalsveien. De som fikk bygd hus hadde lett for å skaffe seg losjerende. Fabrikkene selv bygde hus rundt fabrikken der arbeiderne kunen få losji. Rundt fabrikkene ble det skapt et lokalsamfunn med tette bånd mellom de 2000 menneskene som bodde og jobbet i Nydalen.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_image081.jpg', 'text': '', 'alttext': 'Arbeiderboliger i Nydalen', 'width': 237, 'height': 158}, 'title': 'Arbeiderboliger i Nydalen', 'url': 'http://www.industrimuseum.no/17_arbeiderboliger_nydalen', 'coords': '59.95218999316261, 10.767545700073242', 'audio': ['http://www.industrimuseum.no/filearchive/024_Arbeiderboliger.mp3']}, {'description': 'Når vi i dag står på Gullhaug Torg er vi i sentrum av det gamle Spigerverket som startet i 1853 og ble nedlagt i 1989. Den gang het plassen Bikkjetorget - kanskje fordi det var en vakthund her? Her sto nemlig vaktbua til Spigerverket, en lav murbygning med klokketårn midt på taket og vinduer hele veien rundt. Arbeidere og funksjonærer passerte vakta hver morgen og ettermiddag. Det var store porter inn til de ulike avdelingene - de store smelteverkshallene, valseverkene, redskapsfabrikken, spikerklipperiene. De fleste av disse hallene er revet. Den gamle redskapsfabrikken er bevart. Riksteaterets Osloscene er i en av de nyere bygningene fra 1960-tallet. Når vi ser mot øst kan vi forestille oss de store smelteverkshallene: De nye kontorbyggene med TV2s studioer lengst oppe i rekka har omtrent samme beliggenhet, retning og høyde. Ser vi sørover nedover elva ser vi betongkonstruksjoner i elva som er rester etter galvaniserimgsbyggene som lå over elva. Disse ble revet for å åpne opp elva og gi plass til nybygg.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_image027.png', 'text': '', 'alttext': 'Bikkjetorget rett før vaktbua ble revet ca. 1962', 'width': 238, 'height': 185}, 'title': 'Christiania Spigerverk (I)', 'url': 'http://www.industrimuseum.no/bedrifter/christianiaspigerverk', 'coords': '59.95005718761039, 10.765850549182104', 'audio': ['http://www.industrimuseum.no/filearchive/017_Spigerverket.mp3', 'http://www.industrimuseum.no/filearchive/018_Spigerverket__Duc_.mp3']}, {'description': 'Radiostjernen Rolf Kirkvaag i NRK lagde i 1950 et program om industrien i Olso i forbindelse med 900-årsjubileet for byen. Tittelen på programmet var "Byen i arbeid". ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_sommerfelt_NTM%20CS%20319.jpg', 'text': 'Christian Sommerfelt ved Spigerverket ble intervjuet av Kirkvaag', 'alttext': 'Christian Sommerfelt, Christiania Spigerverk (Ingressbilde)', 'width': 195, 'height': 243}, 'title': 'Byen i arbeid 1950: Rolf Kirkvaag på Spigerverket', 'url': 'http://www.industrimuseum.no/kirkvaag_spigerverket', 'coords': '59.94972407571039, 10.765314102172851', 'audio': ['http://www.industrimuseum.no/filearchive/NRK_oslo900_kirkvaag_spigerverket.mp3']}, {'description': '"Finn og Gro må gå - la Spigerverket bestå!" Dette var parolen rettet mot industriminister Finn Christensen og Statsminister Gro Harlem Brundtland da hundrevis av sinte og skuffede Spigerverksarbeidere samlet seg i "Spikersuppa" 23 mai 1989. Denne parken med fonteneanlegg mellom Stortinget og Nationathteateret har navn etter Christiania Spigerverk i 1953 hadde skjenket byen i stitt 100-årsjubileumsår. Nå skulle Stortingen beslutte en plan som innebar at Spigerverket i Oslo skulle legges ned.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_PICT0057.JPG', 'text': 'Forsiden av avisen Klassekampen slo fast at Oslo Ap med Thorbjørn Berntsen i spissen hadde svikta Spigerverket. ', 'alttext': 'Avisklipp', 'width': 201, 'height': 243}, 'title': 'Finn og Gro må gå - la Spigerverket bestå!', 'url': 'http://www.industrimuseum.no/spigersuppedemo1989', 'coords': '59.94987450730122, 10.765260457992553', 'audio': ['http://www.industrimuseum.no/filearchive/NRK_herognaa_1989_klipp_spigerverknedleggelse_demoSpigersuppa.mp3']}, {'description': 'Redskapsfabrikken er den lave mursteinsbygningen midt i Nydalsbyen. Fiskars produserer fortsatt snøskuffer og spader her. Over den sørligste gavlen troner fortsatt en stor jernring med to store bokstaver: CS. Bygningen ble reist i mange etapper fra 1877. Spigerverket hadde ekspandert jevnt på østsiden av elva i over 20 år, og trengte nå et nytt fabrikkbygg. Her ble det samlet over 40 klippemaskiner for spiker som ble drevet av kraft fra elva. I 1912 startet Spigerverket egen produksjon av redskaper. En fabrikk lenger nede i elva, i gamle Bentsebrugs papirfabrikk, ble kjøpt og utstyr og folk flyttet til Nydalen. "Norges-spaden" ble et kjent varemerke. Etter hvert overtok redskapsproduksjonen hele bygningen mens spikerproduksjonen ble flyttet til stadig større og mer moderne lokaler.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20147.jpg', 'text': '', 'alttext': 'Fiskars redskapsfabrikken', 'width': 237, 'height': 178}, 'title': 'Redskapsfabrikken', 'url': 'http://www.industrimuseum.no/16_redskapsfabrikken', 'coords': '59.95090601982181, 10.764037370681762', 'audio': ['http://www.industrimuseum.no/filearchive/021_Redskapsfabrikken__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/redskapsfabrikken_skafting.mp3']}, {'description': 'Da Spigerverket ble startet i 1853 var det både møller og tresliperi i Nydalen. Den omstridte predikanten og fattigmannsvennen Hans Nielsen Hauge startet Bakke mølle i 1811 etter et av sine fengselsopphold. (Det var ulovlig å drive forkynning utenfor kirkene.) Mølla skulle skaffe brød til byens mange fattige. Bakke Mølle brant flere ganger, og ble til slutt overtatt av Bjølsen Valsemølle. Etter hvert ble mølla helt omringet av det ekspanderende Spigerverket, og i 1941 ble den kjøpt opp og lagt ned. Spigerverket tok den i bruk som lager og rasjonaliseringskontor.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20137.jpg', 'text': '', 'alttext': 'Bakke Møle', 'width': 237, 'height': 155}, 'title': 'Bakke Møle', 'url': 'http://www.industrimuseum.no/bakkemolle', 'coords': '59.94847224349006, 10.764654278755188', 'audio': ['http://www.industrimuseum.no/filearchive/022_Bakke_M_lle.mp3', 'http://www.industrimuseum.no/filearchive/023_Bakke_M_lle__Duc_.mp3']},   {'description': 'Bjølsen Valsemølle fra 1884 er den lengstvarige produksjonen ved Akerselva, og representerer også den lengste tradisjonen for hva elva har vært brukt til: Maling av korn. Dette ble fallene i Akerselva brukt til alt i middelalderen. Dagens produksjon ved Regal Møller foregår i de samme lokalene som i 1884.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-085.jpg', 'text': '', 'alttext': 'Bjøsen Valsemøle mot Sandakerveien', 'width': 237, 'height': 168}, 'title': 'Bjøsen Valsemøle A/s', 'url': 'http://www.industrimuseum.no/bedrifter/bjoelsenvalsemoellea_s', 'coords': '59.94040633833225, 10.769126320046988', 'audio': ['http://www.industrimuseum.no/filearchive/NRKostlandssendingen_bjolsenvalsemolle_1980klipp.mp3', 'http://www.industrimuseum.no/filearchive/026_Bj_lsen_valsem_lle.mp3']}, {'description': 'I 2005 forsvant en meget karakteristisk lukt fra byen: Lukten av gjær hadde vært godt merkbar langt utenfor Bjølsen-området, om vinden var slik. For mange var det en plagsom lukt, og noe som ble brukt som argument for å legge ned fabrikken. Andre sørget over at en gammel og særnorsk levende gjærkultur som var grunnstammen i produksjonen forsvant, og mente et viktig element i tradisjonelt bakverk ble borte! ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_idun2-1..jpg', 'text': 'Fabrikkbygningen til Idun Industri A/S, Treschowsgt. 1, Oslo, revet ca. 2007', 'alttext': 'idun2 (Ingressbilde)', 'width': 237, 'height': 158}, 'title': 'De norske Gj\xe6r- & Spritfabrikker A/S', 'url': 'http://www.industrimuseum.no/bedrifter/denorskegjaer__spritfabrikkera_s', 'coords': '59.9414636, 10.7678538', 'audio': ['http://www.industrimuseum.no/filearchive/031_Farvel_til_norsk_gj_rstamme_.mp3', 'http://www.industrimuseum.no/filearchive/032_Farvel_til_norsk_gj_rstamme___Duc.mp3']}, {'description': 'Mellom Treschows gate og Sandaker har det blitt spent en flott bru med navnet Jerusalem Bru. Dette var Sagenes tusenårsprosjekt, og den åpnet høsten 2011. Den er en del av sykkelveinettet fra Bestumkilen til Sinsen. Navnet stammer fra en husmannsplass og en tidligere papirmølle som lå på nedsiden av den nye brua, på LIlleborgs område. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-078.jpg', 'text': '', 'alttext': 'Jerusalem bru over elva sett mot øst og Bjøsen Valsemøle', 'width': 237, 'height': 178}, 'title': 'Utsikt fra Jerusalem br', 'url': 'http://www.industrimuseum.no/22_jerusalembru_tekst', 'coords': '59.93940176375803, 10.766665935516357', 'audio': ['http://www.industrimuseum.no/filearchive/029_Utsikt_fra_Jerusalem__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/030_Utsikt_fra_Jerusalem.mp3']},  {'description': 'Ved Akerselva mellom Bjølsen og Bentse Brug hadde det vært stor virksomhet også på 1700-tallet. På plassen Jerusalem ble det bygd papirmølle. Gerhard Treschow som eide Bjølsen gård hadde tre sager på vestsiden av den kraftige Bjølsenfossen. Han startet såpekokeri og linoljemølle rett ved. I 1811 ble området rundt plassen Jerusalem hetende Lilleborg. Det kom til noe tekstilproduksjon og et sagbruk. Den drifitge apotekeren Peter Møller kjøpte tekstilfabrikken i 1829. Han startet spinneri, oljemølle og såpekokeri, og drev oppdtett av blodigler til medisinsk bruk. Her fant han også en ny metode for tranproduksjon med damp som gjorde "Møllers Tran" til et landskjent varemerke. Men Lilleborg skulle bli mest kjent som såpe. Industrimannen Peter Kildahl overtok Lilleborg-anleggene i 1863, og satset fra da for fullt på såpefabrikk. I tillegg ble oljemøllen videreført for å gi råstoff både til såpe og til maling- og lakkindustrien.\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-073.jpg', 'text': '', 'alttext': 'Lilleborg med utekafeer og boliker', 'width': 182, 'height': 243}, 'title': 'Lilleborg- mer enn såpe!', 'url': 'http://www.industrimuseum.no/25lilleborg_tekst', 'coords': '59.93784857144112, 10.762288570404052', 'audio': ['http://www.industrimuseum.no/filearchive/033_Lilleborg_-_mer_enn_s_pe_.mp3']}, {'description': 'Lilleborg AS ble stiftet i 1897 som en videreutvikling av Kildahls Lilleborg. Det nye selskapet viste at Lilleborg var blitt en storindustri med internasjonale kontakter og moderne merkevarestrategier og stykkemballasje. Merker som Blenda, Zalo, Lano, Krystal og Solidox ble etter hvert innarbeidet i Norge av Lilleborg, sammen med en rekke internasjonale merkevarer som Lilleborg fikk lisens for gjennom samarbeid med Unilever, som også ble Lilleborgs største eier mellom 1930 og 1959 gjennom oljeprodusenten Denofa i Fredrikstad. Borregaard kjøpte selskapene i 1959 og slo dem sammen til Denofa Lilleborg, som fra 1986 ble en del av Orkla. \r\nLilleborg hadde i 2006 tre fabrikker i Norge, to i Kristiansund-området, og en stor fabrikk i Ski, hvor produksjonen i Østlandsområdet ble samlet i 1997. Samtidig ble produksjonen i Oslo avviklet, og fabrikkområdet ved Akerselva utviklet til boligområde. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-072.jpg', 'text': '', 'alttext': 'Lilleborg med boligstrøk av ombygde industribygg og nybygg', 'width': 237, 'height': 178}, 'title': 'Lilleborg Fabriker A/s', 'url': 'http://www.industrimuseum.no/bedrifter/lilleborgfabrikera_s', 'coords': '59.9373098, 10.7657862', 'audio': ['http://www.industrimuseum.no/filearchive/034_Lilleborgs_s_pefabrikk.mp3']}, {'description': 'Treschow og Bentsen – dette er navnene som rammer inn Akerselva fra Treschows Gate og Treschows Bru til Bentsebrua, fra Bjølsenfossen til Vøyen. Her var det stor aktivitet også på 1700-tallet – og kamp mellom to driftige gründere. Bentsen hadde ideer om moderne papirproduksjon, og Treschow finansierte fabrikken. Kompaniskapet var turbulent og endte i retten. Ingen av dem ble rike på papir.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_bjolsen_OB_F01691.jpg', 'text': 'Bjøsen Hovedgård som Treschow eide', 'alttext': 'Bjøsen Hovedgård (Ingressbilde)', 'width': 237, 'height': 176}, 'title': 'Gr\xfcnderkamp', 'url': 'http://www.industrimuseum.no/21grunderkamp_tekst', 'coords': '59.93953074531565, 10.764455795288086', 'audio': ['http://www.industrimuseum.no/filearchive/027_Gr_nderkamp.mp3']}, {'description': 'Mellom Bentsebrua og Lilleborg troner Lilleborgs lagerbygg fra sekstitallet. Her lå Bentse Brug - den første ordentlige papirfabrikken ved Akerselva. Den ble nedlagt i 1899 etter 200 turbulente år. Ole Bentsen startet moderne papirproduksjon etter hollandsk mønster her i 1696. Det var håndverksbasert produksjon av tekstilpapir. Etter få år ble Bensten skvist ut av sin kompanjong Treschow. På folkemunne og med senere eiere var det likevel Bentsens navn som ble værende på bruket.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-069.jpg', 'text': 'I strykene på oversiden av Bentsebrua kan man se rester etter vanninntakskanalene til Bentse Brug. Bygningene ble revet på 1960-tallet.', 'alttext': 'Over Benstebrua, der Benste Brug lå. Bygningene revet ca. 1960', 'width': 237, 'height': 178}, 'title': 'Bentse Brug - industrielt gjennombrudd for papir', 'url': 'http://www.industrimuseum.no/27bentsebrug', 'coords': '59.936956393479534, 10.761666297912597', 'audio': ['http://www.industrimuseum.no/filearchive/035_Bentse_Brug_-_industrielt_gjennombrudd_for_papir.mp3']}, {'description': 'Myrens Mekaniske verksted ved Sagene i Oslo var fabrikkenes fabrikk. Myrens produserte turbiner, dampamskiner, sagbruks- og høvlerimaskiner og en mengde annet utstyr for industrien, og ble særlig kjent som en av verdens ledende produsenter av treforedlingsutstyr. Det var over 1000 ansatte på verket alt i 1890. Myrens startet Fredrikstad Mek. verksted, samarbeidet med Karlstad Mek Verksted og ble kjøpt av Kværner i 1928. Fabrkken på Myren ble stadig utvidet etter 1950, og var en høyt spesialisert tungindustribedrift som hadde et godt rykte på verdensmarkedet . I 1988 ble produksjonen flyttet til Lier da eierselskapet Kværner samlet Myren, Thune, Eureka og andre datterselskaper. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_C%2022595.jpg', 'text': 'Interiør fra Myrens Verksted, Foto NTM', 'alttext': 'myren (Ingressbilde)', 'width': 237, 'height': 176}, 'title': 'Myrens Verksted A/S', 'url': 'http://www.industrimuseum.no/bedrifter/myrensverksteda_s', 'coords': '59.93473094141863, 10.759505503179975', 'audio': ['http://www.industrimuseum.no/filearchive/037_Myrens_verksted.mp3', 'http://www.industrimuseum.no/filearchive/038_Myrens_verksted__Duc_.mp3']}, {'description': 'Nedenfor Bentsebrua ligger Myraløkka, parkmessig og åpent som et amfi, vis-\xe0-vis Myrens Verksted. Landskapet er ikke naturlig, men ble formet slik etter utvinning av leire til den omfattende teglsteinsproduksjon i siste halvdel av 1800-tallet. Det var byggeboom, og mursteinene fra Bentse Teglverk rakk knapt å tørke før de ble til vegger i Kristianias mange bygårder. Etter at byggeboomen la seg og teglverket ble nedlagt kjøpte Oslo kommune området for å lage park. Bortsett fra utgravningene i landskapet flere steder langs elva er det ingen spor etter tegsletinsproduksjonen. Ett unntak finnes her på vestre bredd av elva: Under en av haugene ligger en av skorsteinene fra teglverket begravd som fyllmasse! Kanskje fremtidens arkeologer vil lure på hvorfor vi har begravd den?', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-057.jpg', 'text': '', 'alttext': 'Myren sett fra Maridalsveien mot Bentsebrua', 'width': 237, 'height': 178}, 'title': 'En industriell gravhaug', 'url': 'http://www.industrimuseum.no/31industrigravhaug', 'coords': '59.93641892529157, 10.75735330581665', 'audio': ['http://www.industrimuseum.no/filearchive/039_En_industriell_gravhaug.mp3']}, {'description': 'Dramaserien Hotell Cæsar har vært en av TV2s største suksesser og norsk TV-produksjons lengstlevende serie. Hotell Cæcar produseres i de tidligere verkstedhallene til Myrens Mekansike Verksted som la ned i 1988. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_myrens%20002.jpg', 'text': '', 'alttext': 'myrens 002.jpg', 'width': 238, 'height': 181}, 'title': 'Rom for alt!', 'url': 'http://www.industrimuseum.no/32caesar_tekst', 'coords': '59.93443021725832, 10.758254528045654', 'audio': ['http://www.industrimuseum.no/filearchive/040_Rom_for_alt_.mp3']}, {'description': 'Oppe på vestre brink over Myraløkka ligger en rad med fargerike trehus. Det er Maridalsveien 131-95, og følger vi rekka nedover, kommer vi til Griffenfeldts gate og Vøyenbrua. Rekken av gamle hus fortsetter på den andre siden av gata. Flertallet av disse husene er typiske eldre arbeiderboliger knyttet til Akerselva, oppført i første halvdel av 1800-tallet. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-060.jpg', 'text': '', 'alttext': 'Bolighus i Maridalsveien ved Myra, Sagene', 'width': 237, 'height': 178}, 'title': 'Trehusbebyggelsen langs Maridalsveien ', 'url': 'http://www.industrimuseum.no/34_hus_maridalsveien_tekst', 'coords': '59.93482796840457, 10.755980014801025', 'audio': ['http://www.industrimuseum.no/filearchive/041_Trehusbebyggelsen_langs_Maridalsveien.mp3']}, {'description': 'Akerselvas rennende vann var av stor verdi for Oslo, som alle elver, av tre hovedgrunner: Rennende vann gir vannforsyning til mennesker og produksjon, rennende vann gir kraft, og rennende vann kan ta med seg avfall som sagflis, kloakk, fargestoffer og kjemikalier. Akerselva og Maridalsvannet skulle ha nok vann til alle disse oppgavene. Likvel ble det ofte uenighet mellom ulike brukere og behov. Når kommunen ledet stadig mer vann fra elva ut i rør til vannforsyning for byens befolkning ble det mindre kraft til drift av industrien. Når industriutslipp og kloakk forurenset elva ble det ulemper for byens befolkning - og farlig for dyrelivet i elva, som i lange tider har vært nærmest fraværende. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-066.jpg', 'text': 'Vannintaket ved Vøyenbrua', 'alttext': 'Boliger i Maridalsveien ved Myra opp mot Sagene', 'width': 237, 'height': 178}, 'title': 'Vann - ressurskamp og miljø', 'url': 'http://www.industrimuseum.no/37vann_tekst', 'coords': '59.933618575120995, 10.757074356079101', 'audio': ['http://www.industrimuseum.no/filearchive/044_Vann_-_ressurskamp_og_milj___Duc_.mp3']}, {'description': 'Hovedhuset på Nedre Vøyen står fortsatt i Maridalsveien 87, på en flik av en tidligere så romslig hage. Gården var tilknyttet den førindustrielle møllevirksomheten ved Vøyenfallene. Våningshuset fra 1783 oppført i Louis Seize-stil, tilhører den eldre gårdsbebyggelsen i Aker. Veggene av laftetømmer er utvendig forblendet og pusset, og gjør bygningen til forveksling lik et murhus. Også mansardtaket viser at eierne hadde ambisjoner om å gjøre huset ekstra fint og standsmessig. For på Nedre Vøyen var det velstand. Gården med tilstøtende bakbygning ble overtatt av Halvor Schou i 1873. Hovedhuset ble da bolig for fabrikkbestyreren på Hjula veveri, og på tomten forøvrig ble det oppført arbeiderboliger for fabrikkarbeidere og veversker ved bedriften. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-097.jpg', 'text': '', 'alttext': 'Hovedbygningen på Nedre Vøyen gård, Maridalsveien 87 ', 'width': 237, 'height': 178}, 'title': 'Nedre Vøyen Gård', 'url': 'http://www.industrimuseum.no/36nedrevoyen_tekst', 'coords': '59.93234732086548, 10.756543278694152', 'audio': ['http://www.industrimuseum.no/filearchive/043_Nedre_V_yen_g_rd.mp3']},  {'description': 'Den gule trebygningen på østsiden av elva rett ved Vøyenfossen er den eldste bevarte fabrikkbygningen ved Akerselva: Glads mølle fra 1736. Den ble startet som papirmølle av handelshuset Collett & Leuch, og ble drevet som en del av den større papirfabrikken Bentse Brug. Fredrik Glad på Grefsen Gård overtok mølla i 1798 og drev den til denne typen papirproduksjon i liten skala ble avlegs. Fra 1880 ble bygningen overtatt av Hjula Væverier. Bygningen ble fredet i 1967.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-099.jpg', 'text': '', 'alttext': 'Glads Møle sett fra vestre bredd over elva mot øst', 'width': 237, 'height': 175}, 'title': 'Glads Møle - et av Norges eldste bevarte industribygg', 'url': 'http://www.industrimuseum.no/glademolla', 'coords': '59.93187697131995, 10.757954120635986', 'audio': ['http://www.industrimuseum.no/filearchive/akerselvasangen_fanfare_jollykramer.mp3', 'http://www.industrimuseum.no/filearchive/067_Glads_M_lle__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/068_Glads_M_lle.mp3']}, {'description': 'Fra Beyerbrua - fabrikkjentenes bru - ser vi en gammel mursteinsbygning med karakteristiske fasader med trappegavler ut mot elva. Restauranten Månefisken ligger i lokalet nederst ut mot elva. Fabrikken som lå her var Hjula Væveri. Bygningene ved elva har forandret seg overraskende lite siden den gang da bygningen var ny, tegnet av sivilingeniør Oluf Roll på oppdrag av svogeren Halvor Schou. Hjula veveri ble en av de største og mest kjente fabrikkene ved Akerlseva. Ikke bare var merket kjent fra gardin- og kjolestoff, arbeidsklær og sengetøy. Publikum fikk besøke fabrikken på "åpen dag"-arrangementer, Hjula ble malt av kunstnere, diktet om av Oscar Braathen, satt som scene i bøker, skuespill og filmer, og sunget om i en stor slager av Inger Jacobsen på 1950-tallet. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_hula_fabrikkjentene.jpg', 'text': 'NTM 99/1-9', 'alttext': 'Fabrikkjentene foran Hjula (Ingressbilde)', 'width': 159, 'height': 242}, 'title': 'Hjula Veveri', 'url': 'http://www.industrimuseum.no/bedrifter/hjulavaeveri_halvorscho', 'coords': '59.93139317625682, 10.757074356079101', 'audio': ['http://www.industrimuseum.no/filearchive/050_Hjula_V_verier.mp3', 'http://www.industrimuseum.no/filearchive/051_Hjula_V_verier__Duc_.mp3']}, {'description': 'NRK-reporter Odd Nordland samtaler med 84 år gamle Mina Olsen, som arbeidet ved Hjula veveri ca 1892. Hun forteller litt om arbeidsforhold, fritid og familie. Sendt 1. mai 1962.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_HJULA%201950.jpg', 'text': '', 'alttext': 'hj', 'width': 237, 'height': 180}, 'title': 'Fabrikken og gata: NRK-møte i 1960 med tidligere Hjula-arbeiderske.', 'url': 'http://www.industrimuseum.no/NRK_ihjulaarbeiderske', 'coords': '59.93153159612041, 10.755912959575653', 'audio': ['http://www.industrimuseum.no/filearchive/NRK%20hjulaarbeiderske%20Mina%20Olsen%201960.mp3']}, {'description': 'Danske Knud Graah kom til Kristiania som 16-åring i 1833, jobbet seg opp i handelsstanden og ble en foregangsmann for norsk tekstilindustri. Han etablerte Vøiens Bomuldsspinderie i 1846 sammen med sin svoger, grosserer Niels Young. Graah hadde, på samme måte som tekstilgrÅnderne Schou og Hiorth, hatt flere studieturer til Manchester og andre ledende industribyer. Teknologi, arkitektur og ekspertise ble importert fra England. Fabrikken ble stadig utvidet. Etter en brann i 1859 fikk han hjelp av industriarkitekten Roll til å bygge et dobbeltså høyt bygg som ble et ruvende symbol på industrireisingen i sin samtid. De imponerende fabrikkbygningene står fortsatt som et av mest imponerende industriminnene langs Akerselva, i dag best kjent som Mølla, etter restauranthuset som tok plass i bygningen etter 1980. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_graas_naa.jpg', 'text': 'Vøyenvossen med Graahs spinenri, fotografert i 1999', 'alttext': 'vøyen1999 (Ingressbilde)', 'width': 237, 'height': 159}, 'title': 'Knud Graah & Co A/S', 'url': 'http://www.industrimuseum.no/bedrifter/a_sknudgraah_co', 'coords': '59.930678221746206, 10.756645202636718', 'audio': ['http://www.industrimuseum.no/filearchive/057_Graah__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/058_Graah.mp3']}, {'description': 'Tre søstre som bodde på "Rivertzke" på Sagene jobbet samtidig på Graah. De to yngste ble intervjuet i 1999 og fortalte om arbeidsdagene på tekstilfabrikken i 1930-årene.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-012.jpg', 'text': '', 'alttext': 'Islagte Vøyenfossen med "Møla" - Graahs spinneri', 'width': 182, 'height': 243}, 'title': 'Jobbe på Graah', 'url': 'http://www.industrimuseum.no/45_jobbegraah_tekst', 'coords': '59.93024816649802, 10.75710654258728', 'audio': ['http://www.industrimuseum.no/filearchive/059_Jobbe_p__Graah__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/060_Jobbe_p__Graah.mp3']}, {'description': 'Beierbrua er på mange måter Oskar Braatens bro. Han kalte den fabrikkjentenes bro. I oppveksten var han daglig vitne til den strie strømmen av kvinner til eller fra arbeidet i fabrikkene. Oskar Braaten var Oslos arbeiderdikter født i 1881 og død i 1939. Han skrev om Johnny og Mathilde i Ulvehiet, om Hønse-Lovisa og Milja og alle de andre fabrikkjentene som hadde havnet i \xabuløkka\xbb, om sorger og gleder i arbeiderstrøkene rundt Sagene og andre steder. Han har fått sin byste oppsatt her, og selv bodde han oppe i Holsts gate 2 på hjørnet av Sandakerveien i det han kalte for Hjemmet. Du ser den hvite leiegården fra brua. Her vokste han opp med søsteren og moren. I 1928 skrev Braaten: "At det ikke finnes noen mer bediktet gatestump i Oslo by enn Sandakerveien fra Beierbroen og op til Thorshauggaten, tror jeg nok jeg tør si."', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-093.jpg', 'text': '', 'alttext': 'Sagveien mot Hjula Veveri', 'width': 237, 'height': 175}, 'title': 'Oskar Braatens univers', 'url': 'http://www.industrimuseum.no/43braathensunivers_tekst', 'coords': '59.9309416278322, 10.75834035873413', 'audio': ['http://www.industrimuseum.no/filearchive/055_Oskar_Braatens_univers.mp3', 'http://www.industrimuseum.no/filearchive/056_Oskar_Braatens_univers__Duc_.mp3']}, {'description': 'Trebrua over elva ved Hønse-Lovisas hus er en av de eldste broene. Den har navn etter eier i 1671, skredder Anders Beyer. Første brua var en enkel gangbro, men i 1837 ble den utvidet til kjørebro. Utover på 1900-tallet trillet det biler her også, men i 1974 brant broa. Da ble den restaurert tilbake til 1837-utgaven av seg selv, og er nå kun gangbro igjen. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20098.jpg', 'text': '', 'alttext': 'Fabrikkjentene og Hjula', 'width': 237, 'height': 178}, 'title': 'Beierbrua - fabrikkjentenes br', 'url': 'http://www.industrimuseum.no/41_beierbrua_tekst', 'coords': '59.93096447417987, 10.75747400522232', 'audio': ['http://www.industrimuseum.no/filearchive/052_Beierbrua_-_fabrikkjentenes_bru__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/053_Beierbrua_-_fabrikkjentenes_bru.mp3']}, {'description': 'Hønse-Lovisas hus har fått sitt navn etter en skikkelse i Oskar Braatens skuespill "Ungen" fra 1911. Hønse-Lovisa er en modig og generøs kvinne som tar seg av barna til jentene som arbeidet på fabrikkene langs elva. Huset ble sannsynligvis satt opp som sagmesterbolig omkring 1800, og har vært både bolig for mestre og arbeidere, lager og butikk. I falleferdig tilstand ble den tatt i bruk av kunststudenter før en forfatter satte huset i stand i samarbeid med kommunen. I dag drives stedet som en liten kulturkaf\xe9 hvor historie møter diktning og samtidskunst. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20096.jpg', 'text': '', 'alttext': 'Hønse-Lovisas hus', 'width': 237, 'height': 178}, 'title': 'Hønse-Lovisas hus', 'url': 'http://www.industrimuseum.no/48_honselovisa', 'coords': '59.930830083673406, 10.757592022418975', 'audio': ['http://www.industrimuseum.no/filearchive/064_H_nse-Lovisas_hus.mp3', 'http://www.industrimuseum.no/filearchive/065_H_nse-Lovisas_hus__Duc_.mp3']}, {'description': 'En flott fabrikkpipe med en ring med store "R"-logoer står igjen som et godt synlig spor etter arnestedet for det som i dag er Norges største bryggerikonsern: Ringnes. Bryggeriet ble grunnlagt i 1876 av Amund og Ellef Ringnes og konsul Axel Heiberg. Fabrikken på Sagene ble stadig utvidet, og var produksjonssted for øl helt til 2001. Etter at Ringnes flyttet all produksjon til Gjelleråsen ble mye av bryggeriet revet for å gi plass til boliger, forretninger og kino. Noen av de eldste og mest karakteristiske fabrikkbygningene - og pipa - er bevart. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_ringnes.jpg', 'text': 'Ringnes-anlegget under ombygging til boliger i 2006', 'alttext': 'ringnes.jpg', 'width': 237, 'height': 178}, 'title': 'Ringnes Bryggeri A/s', 'url': 'http://www.industrimuseum.no/bedrifter/ringnesbryggeria_s', 'coords': '59.929904118285705, 10.759692192077636', 'audio': ['http://www.industrimuseum.no/filearchive/071_Ringnes.mp3']}, {'description': 'Sagveien 8 går under navnet "Brenna" og er et svalgangshus i tre fra 1848, oppført av fabrikkeier Knut Graah. Boligbygging på 1800-tallet foregikk stort sett i privat regi, på intiativ fra fabrikkeiere og boligspekulanter. \xabBrenna\xbb er sjelden i sitt slag fordi slike arbeiderboliger i tre etasjer eller mer ganske snart ble bygget i rød teglsten, akkurat som fabrikkene. Sagveien 8, i motsetning til flere av de lavere tre- og teglverkshusene i strøket, var bygget for rent boligformål, altså ingen tilsluttet næringsvirksomhet. I huset er det 12 leiligheter, alle så nær som en; på ett rom og kjøkken. I 1875 bodde det 63 personer her. Uthusene, opprinnelig to, inneholdt doer og vedskjul til hver leilighet, og i bakgården var det vannspring. Oslo Museum forvalter en museumsleilighet i 1.etg og formidler industri- og arbeider historie herfra, særlig til skoleklasser i Oslo og omegn. De øvrige leilighetene er i 2012 selveierleiligheter, fortsatt små men med moderne fasiliteter. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-092.jpg', 'text': '', 'alttext': 'Brenna i Sagveien 8 med Oslo Museums leilighet', 'width': 237, 'height': 187}, 'title': 'Brenna - en arbeiderbolig i Sagveien 8', 'url': 'http://www.industrimuseum.no/46brenna_tekst', 'coords': '59.9298988, 10.754675', 'audio': ['http://www.industrimuseum.no/filearchive/061_Brenna_-_en_arbeiderbolig_i_Sagveien_8.mp3', 'http://www.industrimuseum.no/filearchive/062_Brenna_-_en_arbeiderbolig_i_Sagveien_8__Duc_.mp3']}, {'description': 'Dette huset fra 1872 skiller seg ut i mengden i likhet med apoteket i Sagveien 28. Murvillaen ble oppført som et \xe9nfamiliehus midt på tomten, et riktig velstandshus, opprinnelig eid av fabrikkbestyrer Lohrbauer ved Christiania Mekaniske Industri. Boligen hadde en frodig hage, mange tapetserte rom, fem kakkelovner til oppvarming, innlagt vann og en nydelig treveranda som ga huset karakter. Etter hvert som behovet for flere boliger i strøket økte, ble huset befolket av flere familier, men det var etter at bestyreren var flyttet ut!', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_sagv24_NTM%20C%2022611.jpg', 'text': 'Foto: Norsk Teknisk Museum / Finn LArsen 1977 NTM C 22611', 'alttext': 'Sagveien 24 - østgavlen i 1977 (Ingressbilde)', 'width': 241, 'height': 243}, 'title': 'Fabrikkbestyrerbolig i Sagveien 24', 'url': 'http://www.industrimuseum.no/49fabrikkbestyrerbolig_sagv24', 'coords': '59.9305177, 10.7553691', 'audio': ['http://www.industrimuseum.no/filearchive/066_Fabrikkbestyrerbolig_i_Sagveien_24.mp3']}, {'description': 'Sagene Apothek ble oppført i 1870 av apoteker Lindgaard som et lite bypal\xe9 her i fabrikk- og arbeiderstrøket. Det fantes både brann- og politistasjon, øl- og vedutsalg, samt flere fedevare- og kolonialvarehandlere i området men et apotek manglet. Huset ble innredet med apoteket i første etasje og en romslig leilighet for apotekerfamilien i annen etasje. I 1875 bodde, foruten familien på seks, en disippel, en farmasøyt, to tjenestepiker og en tjenestedreng også her. Apoteket ble drevet fram til 1905.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-095.jpg', 'text': '', 'alttext': 'Apotekergården i Sagveien, rett ved Hjula og Graah', 'width': 236, 'height': 150}, 'title': ' Det gamle apoteket i Sagveien 28', 'url': 'http://www.industrimuseum.no/47_apoteket', 'coords': '59.93088652775244, 10.755819082260131', 'audio': ['http://www.industrimuseum.no/filearchive/063_Det_gamle_apoteket_i_Sagveien_28.mp3']}, {'description': 'Dette er eksempel på riktig gammel bebyggelse, karakteristisk som trebygningen er med en lav loftsetasje som springer fram en stokkbredde, båret av utstikkende loftsbjelker. Dette trekket er et motefenomen hentet fra danske og tyske byhus, formidlet til Norge gjennom bindingsverkshusene i nede i Christiania. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-089.jpg', 'text': '', 'alttext': 'Bolighus i Maridalsveien', 'width': 237, 'height': 178}, 'title': 'Biermannsgården i Maridalsveien 78', 'url': 'http://www.industrimuseum.no/35biermann_tekst', 'coords': '59.93164045017073, 10.754746198654174', 'audio': ['http://www.industrimuseum.no/filearchive/042_Biermannsg_rden_i_Maridalsveien_78-1..mp3']},  {'description': '\xabVandet i Elven er ganske stinkende og bedærvet og utjenligt til menneskelig bruk\xbb stod det å lese i et innlegg i Morgenbladet allerede i 1861. Fra dag \xe9n slapp industrien avfall ut i Akerselva i form av kjemikalier, bleke- og fargestoffer, hestemøkk og andre uhumskheter. Og ikke bare det; mye av byens kloakk fant også veien ut i Akerselva. Ved århundreskiftet var stanken i elven blitt så sterk at det ble nødvendig å sette i gang en undersøkelse av vannet. Særlig ille var det på søndager og andre helligdager da Brukseierforeningen stengte vannet øverst ved Maridalsoset for å øke vannmengden og trykket ellers i uken. Kommunen og brukseierne klarte aldri å få til en løsning på forurensningsproblematikken. Derfor forble Akerselva \xabdu gamle du grå\xbb inntil industrien var nedlagt, kloakkerensingen fikk et visst omfang frem mot 1980-årene, og man begynte en opptrapping av arbeidet med å fullføre de gamle planene om parker, og turveier og grøntdrag langs Akerselvas bredder. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20044.jpg', 'text': '', 'alttext': 'Fjerdingen', 'width': 237, 'height': 178}, 'title': 'Elvas forurensing', 'url': 'http://www.industrimuseum.no/38_elvasforurensing', 'coords': '59.91728476026652, 10.761398077011108', 'audio': ['http://www.industrimuseum.no/filearchive/046_Elvas_forurensning__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/047_Elvas_forurensning.mp3']}, {'description': 'Barn var attraktiv arbeidskraft i fabrikkene. De var fleksible, fysisk smidige og kunne komme til overalt hvor det var nødvendig. Dessuten var de billig arbeidskraft. En landsoversikt fra 1878 viser at i noen grener av industrien utgjorde barn en meget stor andel av arbeidsstokken. 45 % i tobakksindustrien, 30 % i fyrstikkfabrikkene, og på fotografier over de ansatte ved tekstilfabrikker som Nydalen Compagnie og Seilduksfabrikken, er også store barneflokker i møkkete arbeidsantrekk stilt opp. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_NTM2427%20NCarbediere1877.jpg', 'text': 'Barna foran: Ansatte ved Nydalens Compagnie ca. 1977', 'alttext': 'Arbeidere ved Nydalens Compagnie ca. 1877 (Ingressbilde)', 'width': 236, 'height': 144}, 'title': 'Barnearbeid', 'url': 'http://www.industrimuseum.no/43barnearbeid', 'coords': '59.954596637642354, 10.764970779418945', 'audio': ['http://www.industrimuseum.no/filearchive/054_Barnearbeid.mp3']}, {'description': 'Firmaet ble etablert i 1837 som kammakeri. Senere ble firmaet utvidet til børstebinderi. Det importerte tresorter og produserte sigaresker (skilt ut som egen virksomhet i 1879). Firmaet staret opp i Skippergt. 44, men kjøpte Holms Hattefabrikk Wm. Thranesgt. i 1917. I tillegg startet firmaet egen produksjon av tannbørster, toalettbørster og kammer i ny fabrikk i Ulvenveien 45 på Løren. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_vulkan_foss_0039_edited-1.jpg', 'text': '', 'alttext': 'Jordanfabrikken, Waldemars Hage (Ingressbilde)', 'width': 237, 'height': 174}, 'title': 'W. Jordan Børste & Penselfabrik A/S', 'url': 'http://www.industrimuseum.no/bedrifter/w_jordanboerste_penselfabrika_s', 'coords': '59.92757094719824, 10.753008127212524', 'audio': ['http://www.industrimuseum.no/filearchive/077_Hatter__b_rster_og_mekanikk.mp3', 'http://www.industrimuseum.no/filearchive/078_Hatter__b_rster_og_mekanikk__Duc_.mp3']}, {'description': 'Hvem er det som tramper på min bru? Aamodt bru kunne nesten være hentet ut av eventyret, og inskripsjonen gir ytterligere liv til fantasien: ”100 Mand kan jeg bære, men svigter under taktfast Marsch”. Skiltet var alvorlig ment som en advarsel da brua var ny. En fransk bru av samme konstruksjon hadde rast sammen da et regiment soldater marsjerte over den i 1850. Den taktfaste marsjen skapte svingninger som skal ha vært en av årsakene til at brua brast. At frigang var å foretrekke framfor soldatmarsj har siden blitt tolket symbolsk. Lillebjørn Nilsen har sunget om både skiltet og brua i sangen ""Far har fortalt."', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-008.jpg', 'text': '', 'alttext': 'Aamodt bru i vinterdrakt', 'width': 237, 'height': 178}, 'title': 'Aamodt bru - et kulturminne på avveie?', 'url': 'http://www.industrimuseum.no/59_aamodtbru_tekst', 'coords': '59.92622150377305, 10.75287938117981', 'audio': ['http://www.industrimuseum.no/filearchive/079_Aamodt_bru_-_et_kulturminne_p__avveie___Duc_.mp3', 'http://www.industrimuseum.no/filearchive/080_Aamodt_bru_-_et_kulturminne_p__avveie_.mp3']}, {'description': 'Christiania Seildugsfabrik fra 1856 er en av de flotteste farbikkbygningene ved Akerlseva, med sin beliggenhet øverst på GrÅnerløkka. Øvre Foss er en av de fem naturlige fossene i Akerselva, og også her ble det tidlig anlagt møller og sager. Agers Mechaniske Værksted ble i 1842 den første industribedriften som i 1842 etablerte seg med nybygg på østsiden rett nedenfor fossen. Akers Mek flyttet til Holmen ved Pipervika for å satse på skipsbygging. Dette ga plass til den nye tekstilfabrikken. "Seildugen" ble oppført \xabetter engelsk mønster\xbb med imponerende bygninger tegnet av arkitekt Peter Høier Holtermann. Nest etter slottet var Seildugsfabrikken den største bygningen \xabi Christiania og Omegn\xbb. Folk kom langveis fra for å beundre den.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20086.jpg', 'text': '', 'alttext': 'Christiania Seildugsfabrikk - kunsthøyskolene', 'width': 237, 'height': 145}, 'title': 'Christiania Seildugsfabrik (Oslo Seildugsfabrik A/S)', 'url': 'http://www.industrimuseum.no/bedrifter/chr_aseildugsfabrik%28osloseildugsfabrika_s', 'coords': '59.925570955661115, 10.754435062408447', 'audio': ['http://www.industrimuseum.no/filearchive/081_Seilduksfabrikken__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/082_Seilduksfabrikken.mp3']}, {'description': 'Radiostjernen Rolf Kirkvaag i NRK lagde i 1950 et program om industrien i Olso i forbindelse med 900-årsjubileet for byen. Tittelen på programmet var "Byen i arbeid".', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_NTM%20C%2026643.jpg', 'text': '', 'alttext': 'seilduken', 'width': 237, 'height': 189}, 'title': 'Byen i arbeid 1950: Rolf Kirkvaag på Seilduksfabrikken', 'url': 'http://www.industrimuseum.no/NRK_seildugen_kirkvaag', 'coords': '59.925570955661115, 10.754435062408447', 'audio': ['http://www.industrimuseum.no/filearchive/NRK%20kirkvaag%20seildugen.mp3']}, {'description': 'Fabrikkene lå tett langs Akerselva. Fabrikkportene var grensen mellom livet i fabrikkene og livet utenfor. Tidlig, tidlig om morgenen gikk en strøm av arbeidere inn alle portene. 12 timer senere gikk strømmen ut. Mange var kvinner. “Det er stilt i fabrikarbeidernes bydel om dagen”, skrev Oskar Braaten. Inne i fabrikkene, bak portene, var det maskinstøy, tekstilstøv, fuktighet og ekstrem varme eller kulde. “Det er jenter fra 15 til 70 år. De eldste er tidligst ute, de kommer bestandig med niste under armen. De yngste kommer sist, øre av søvn, grå av tretthet - foran en arbeidsdag som varer til seks om kvelden.”', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_NTM_C_26637.jpg', 'text': 'Christiania Seilduksfabrikk ', 'alttext': 'ntm (Ingressbilde)', 'width': 238, 'height': 207}, 'title': 'Fabrikkportenes bydel', 'url': 'http://www.industrimuseum.no/Fabrikkportenes_bydel', 'coords': '59.925596493948646, 10.754854828119278', 'audio': ['http://www.industrimuseum.no/filearchive/11_Spor_10_Fabri.mp3']}, {'description': 'Akers Mekaniske Verksted (Agers mechaniske Værksted) ble startet i 1841, og flyttet til et lite bindingsverkshus på østsiden av Akerselva nedenfor Øvre Foss året etter. Deler av de første lokalene er bevart ved de mer dominerende bygningene etter Christiania Seildugsfabrikk .\r\nMennene som startet Agers mechaniske værksted tilhørte hovedstadens grÅndermiljø i 1840- og 50-årene, som var en særlig aktiv periode. Det var menn av forskjellige bakgrunner - militære, mekanikere, forretningsfolk, emedsmenn og akademikere som startet en rekke ulike foretak og fabrikker i denne perioden. \r\nOrlogskaptein Peter S. Steensrup regnes som hovedmannen bak Akers mek. Han hadde en lang karriere bak seg som marineoffiser, og hadde blant annet i ung alder vært skipsfører på Norges første dampskip, Constitutionen. Han hadde også vært med på å bygge opp Marinens hovedverft i Horten. \r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20083.jpg', 'text': '', 'alttext': 'Akers mekaniskes første hus', 'width': 237, 'height': 178}, 'title': 'Akers Mekaniske verksted - en industrigigants spede begynnelse', 'url': 'http://www.industrimuseum.no/61_akersmekaniske', 'coords': '59.92500104352794, 10.753453373908996', 'audio': ['http://www.industrimuseum.no/filearchive/083_Akers_mekaniske_verksted_-_en_industrigigants_spede_begynnelse.mp3', 'http://www.industrimuseum.no/filearchive/084_Akers_mekaniske_verksted_-_en_industrigigants_spede_begynnelse__Duc_.mp3']}, {'description': 'Øverst oppe på Akersberget på vestre bredd, kneiser bygdekirken Gamle Aker kirke, byens eldste bevarte bygning, fra 1100-tallet. I berget under kirken, var det gruvedrift etter sølv allerede på 1100-tallet og på 1500-tallet. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20075.jpg', 'text': 'Bygningen i Maridalsveien som har porten inn til Akersberget', 'alttext': 'Akersberget', 'width': 237, 'height': 178}, 'title': 'Gruvene i Akersberget', 'url': 'http://www.industrimuseum.no/akersberget', 'coords': '59.924135403543026, 10.750513672828674', 'audio': ['http://www.industrimuseum.no/filearchive/086_Gruvene_i_Akersberget.mp3']},  {'description': 'Dikt av Haakon Bull- Hansen lest av Rolf Søder 27.5.1986. ', 'img': {}, 'title': 'Ved Nedre Foss Møle', 'url': 'http://www.industrimuseum.no/NRK_dikt', 'coords': '59.92191743406537, 10.754982233047485', 'audio': ['http://www.industrimuseum.no/filearchive/nrk%20dikt%20rolf%20soder.mp3']}, {'description': 'På gavlveggen ved Dansens hus står det AA - initialene til ingeniør Aksel Amundsen som startet Vulkan mekaniske verksted og jernstøperi i 1873. I begynnelsen lå Vulkan i Brenneriveien, litt nedover elva. Etter 1900 overtok Vulkan nabotomten i nord av Bagaas Brug som hadde drevet bl.a. sagbruk, teglverk, tresliperi, møbelfabrikk og sementfabrikk.Vulkan spesialiserte seg på produksjon av stålbroer og hadde blant annet store leveranser til jernbanen. De leverte også konstruksjoner til bygg, bl.a. kuppelen til Colosseum kino. Produksjonen ved Akerselva ble nedlagt på slutten av 1950-tallet da betongbroer overtok for jernbroer. Vulkan fortsatte som eiendomsselskap, og leide ut fabrikkhallene til ulike leietakere. Deler av verkstedhallene ble i 1995 omgjort til idrettshall av Oslo kommune. Øvrige deler har bl.a. vært brukt som verksteder for Riksteatret og Riksutstillinger.\r\nNå blir det kompakte industriområdet mellom Maridalsveien og Nedre Foss utviklet med spennende kontraster mellom den gamle industriarkitekturen og moderne arkitektur og nye funksjoner. Vulkan blir gjenskapt som en moderne bydel med kultur, utdanning, mathall og boliger og park ved elva. Inne i en av de gamle verkstedhallene har Kolsås Klatreklubb en av byen største innendørs klatrevegger.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_vulken_foss_0064.JPG', 'text': 'AA-initialene på Mathallen ved Dansens hus.', 'alttext': 'Vulkan (Ingressbilde)', 'width': 237, 'height': 178}, 'title': 'Vulkan A/S', 'url': 'http://www.industrimuseum.no/bedrifter/vulkana_s', 'coords': '59.921722199627695, 10.751643764804033', 'audio': ['http://www.industrimuseum.no/filearchive/091_Vulkan_jernst_peri_og_mekanisk_verksted__Duc_.mp3']}, {'description': 'Christiania Bryggeri ble etablert i 1855 av fire hedmarkinger, og ble gradvis utbygd til et av de største og mest moderne bryggeriene i Christiania, beliggende i Maridalsveien 3 ned mot Akerselva. Her ble Selters og Solo produsert til kommunen kjøpte anlegget i 1971. I dag er det Oslo Byarkiv som dominerer.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-028.jpg', 'text': '', 'alttext': 'Maridalsveien 3, Christiania Bryggeri / Nora, sett fra rundkjøringen i Maridalsveien / Mølerveien / Fredensborgveien ', 'width': 238, 'height': 139}, 'title': 'Christiania Bryggeri', 'url': 'http://www.industrimuseum.no/christianiabryggeri', 'coords': '59.92085006743223, 10.752182006835937', 'audio': ['http://www.industrimuseum.no/filearchive/092_Christiania_bryggeri_og_Nora_Brusfabrikk.mp3']},  {'description': 'Denne fabrikkbygningen blir fortsatt kalt Indigo. Den er reist i tilknytting til Brenneriveien 9 (Blå) på andre siden av elva. Tekstilfabrikken Indigo ble stiftet i 1891, men de trengte større plass og utvidet med dette bygget i 1898. Tekstilfabrikken A/S Indigo spesialiserte seg på trykking av bomullstøy. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_A-20141_Ua_0002_004.jpg', 'text': 'Indigo bygget i 1898, Oslo byarkiv', 'alttext': 'indigo (Ingressbilde)', 'width': 237, 'height': 172}, 'title': 'Indigo', 'url': 'http://www.industrimuseum.no/bedrifter/indigo', 'coords': '59.91981224227158, 10.753265619277954', 'audio': ['http://www.industrimuseum.no/filearchive/20_Spor_19_Indig.mp3']}, {'description': 'Klubbscenen Blå ble startet i 1998, men det har vært mange forskjellige aktiviteter her opp gjennom årene.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_DSC_0235-1..JPG', 'text': 'testedet Blå. Foto: Bjørn Olav Tveit', 'alttext': 'Blå i dag (Ingressbilde)', 'width': 238, 'height': 159}, 'title': 'Hvorfor heter Blå Blå?', 'url': 'http://www.industrimuseum.no/Blaa', 'coords': '59.920027338235236, 10.75285792350769', 'audio': ['http://www.industrimuseum.no/filearchive/15_Spor_14_Hvorf.mp3', 'http://www.industrimuseum.no/filearchive/16_Spor_15_Fra_g.mp3']}, {'description': 'Flere steder langs Akerselva kan vi studere alle trinn i utviklingen fra kummerlige boforhold på 1800-tallet til designerboliger på 2000-tallet. Byggeboomer både på 1890- og 1990-tallet preger områdene, og et finens mange eksempler på gjenbruk av boligfelt til industribygg og tilbake til boliger igjen. Et eksempel er Nedre Gate 8: Her ble et lite trehus revet i 1898 for å gi plass til tobakksfabrikken Hartog. På 2000-tallet var det biligformål som ga størst fortjeeste for eiendomsutviklerne, og igjen ble adressen til bolighus: farbikken ble gjort om til utleieboliger i kategorien "leilighetshotell".', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_naeringsleksikon1938_006.jpg', 'text': 'Bygget ved Akersevla før idnustribygget i Nedre Gate 8 ble bygd i 1899. ', 'alttext': 'Kristiania Kunst- og Metalstøberi, ca 1898', 'width': 237, 'height': 189}, 'title': 'Ny York og Nye Gr\xfcnerløkka', 'url': 'http://www.industrimuseum.no/72_nyyork_tekst', 'coords': '59.91987677120617, 10.754703283309936', 'audio': ['http://www.industrimuseum.no/filearchive/096_Ny_York_og_Nye_Grunerl_kka__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/097_Ny_York_og_Nye_Grunerl_kka.mp3']}, {'description': 'middelbart før byutvidelsen 1858 ble det reist en forstad av rimelige trehus akkurat i denne delen av Gr\xfbnerløkka.\r\n\r\nOmrådet ble humoristisk betegnet \xabNy York\xbb, antagelig på grunn av den amerikanskinspirerte farten i utbyggingen. Bygningene ble oppført i tømmer, og en av grunnene til farten var at en ville komme murtvangen som var innenfor byen i forkjøpet. Av den opprinnelige "Ny York"-bebyggelsen, som i sin tid utgjorde ca. 70 hus, står i dag bare en håndfull hus igjen. Eksempler på disse er Stolmakergata 8 og Øvre gate 2B.\r\n\r\n ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_naeringsleksikon1938_006-1..jpg', 'text': 'Bygget ved Akerselva før idnustribygget i Nedre Gate 8 ble bygd i 1899. ', 'alttext': 'Hus nedre gate 8 (Ingressbilde)', 'width': 237, 'height': 189}, 'title': 'Nedre gate 8', 'url': 'http://www.industrimuseum.no/Nedre_gate8', 'coords': '59.919693938896934, 10.754735469818115', 'audio': ['http://www.industrimuseum.no/filearchive/07_Spor_6_Nedre_.mp3']}, {'description': 'Visste du at ”Sinnataggen” og den store fontenen som står i Vigelandsparken ble støpt her? Her lå nemlig Kristiania Kunst & Metalstøberi. Det ble stiftet av Ernst Poleszynski i 1894 og er regnet for å være "Norges første kunststøperi”. Satsingen på kunst alene tok slutt da støperiet gjorde suksess med Il-ov-an aluminiumsgryter i 1912.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvabilder_dagandreassen2011-12-001.JPG', 'text': '', 'alttext': 'Is på elva ved Nedre Gr\xfcnerløkka mot Indigo, med tobakksfabrikken og Kunststøperiet ', 'width': 238, 'height': 170}, 'title': 'Kristiania Kunst - og Metallstøberi', 'url': 'http://www.industrimuseum.no/sinnataggen', 'coords': '59.91968049530523, 10.755333602428436', 'audio': ['http://www.industrimuseum.no/filearchive/06_Spor_5_Sinnat-1..mp3', 'http://www.industrimuseum.no/filearchive/23_Spor_22_II-O--2..mp3', 'http://www.industrimuseum.no/filearchive/06_Spor_5_Sinnat-1..mp3', 'http://www.industrimuseum.no/filearchive/099_Sinnataggens_f_destue_.mp3']}, {'description': 'Kristiania Kunststøperi fikk en bestilling litt utenom det vanlige en dag under forbudstiden på 1920-tallet. Den kanskje naive verksmesteren Kristian Reistad fortalte om episoden:\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_ilovan_018.jpg', 'text': 'Fra maskinverkstedet, hvor verktøyet lages og repareres', 'alttext': 'Aluminiumsproduksjonen i Moss ca 1947', 'width': 238, 'height': 170}, 'title': 'En bestilling på kanten', 'url': 'http://www.industrimuseum.no/73_ilovanfortelling_tekst', 'coords': '59.919699316331496, 10.755293369293213', 'audio': ['http://www.industrimuseum.no/filearchive/23_Spor_22_II-O--2..mp3']}, {'description': 'På en nedslitt fasade ved Elvebakken finner du et gjenkjennelig blått skilt som forteller at Hausmannsgate 42 er et brukerstyrt byøkologisk boliginitiativ. Hvis du leser videre får du vite at huset ble regulert til bevaring av et enstemmig bystyre 18.juni 2008, men tømt og gjenmurt 11 februar 2010. Skiltet er overraskende underskrevet av Oslo Byes Vel Vel, og med denne signaturen begynner du å forstå at du står ved et virkelig alternativt boligmiljø.\r\nNaboeiendommen Hausmania med sin lukkede gatefasade og fargesprakende sidevegger yrer likevel av liv. Dette alternative kulturhuset har utviklet seg gradvis siden de første kunstnerne flyttet inn i år 2000. Oslo kommune overtok eiendommene i 2004 og hele kvartalet er nå omregulert til bolig, kultur og næring. Ved siden av kunstneratelieer og et lydstudio består Hausmania av Grusomhetens teater, en flerbrukshall, en skatehall, Humla kultur- og revolusjonsverksted og vegankafeen "Den navnløse". ', 'img': {}, 'title': 'HAUSMANIA En alternativ byvirkelighet, en motsats til mainstream', 'url': 'http://www.industrimuseum.no/82_hausmania', 'coords': '59.919263741252564, 10.751999616622924', 'audio': ['http://www.industrimuseum.no/filearchive/100_Hausmania_En_alternativ_byvirkelighet__en_motsats_til_mainstream.mp3']}, {'description': 'Da den første kommunale parken ble anlagt ved Ankerbrua i 1915 var det delvis som et forsøk på å møte problemer med fyll og løsgjengeri i området. Breddene ovenfor og nedenfor Ankerbrua var et yndet oppholdssted for lasaroner og andre som skapte uro og skremte både fastboende, ordensmyndighetene, fabrikkeiere og arbeidere i området. Håpet var at parkene skulle skremme vekk uønskede elementer ved å forskjønne området og la ”skikkelige” folk overta. Problemene forsvant imidlertid ikke av dette. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20055.jpg', 'text': '', 'alttext': 'Mot Kunststøperiet / Il-O-Van og tobakksfabrikken', 'width': 237, 'height': 178}, 'title': 'Plass for alle?', 'url': 'http://www.industrimuseum.no/90_plass_for_alle', 'coords': '59.91848399380967, 10.757728815078735', 'audio': ['http://www.industrimuseum.no/filearchive/107_Plass_for_alle__Duc_.mp3']}, {'description': 'Østre Elvebakke og Vestre Elvebakke er to smale gater på vestsiden av Akerselva i Hausmannsområdet. Vestre Elvebakke besto av tett bebygde bolighus i tre. Bolighusene ble bygd før gatene ble en del av byen i 1858. Her bodde det først og fremst håndverkere, men også arbeidere som arbeidet på fabrikkene langs Akerselva. I dag er det ingen boliger igjen fra den tidlige trehusbebyggelsen i Vestre - og Østre Elvebakke', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_01z01091_2-2..tif', 'text': 'Vestre Elvebakke 1937', 'alttext': '1937 (Ingressbilde)', 'width': 237, 'height': 138}, 'title': 'Vestre Elvebakke', 'url': 'http://www.industrimuseum.no/Vestre_elvebakke', 'coords': '59.91938339053226, 10.753228068351745', 'audio': ['http://www.industrimuseum.no/filearchive/13_Spor_12_Vestr.mp3']}, {'description': 'Det var ikke tilfeldig at Norges første yrkesskole ble plassert på Østre Elvebakke i 1921. Industrien og småvirksomhetene langs elva hadde behov for skolert arbeidskraft; bilmekanikere, snekkere, pølsemakere og hattemakere. I 1925 fikk skolen navnet Kristiania Fag- og Forskole for Håndverk og industri, i 1948 ble navnet endret til Oslo yrkesskole og i 1976 fikk skolen navnet Elvebakken videregående skole som den fortsatt heter. I dag er Elvebakken er en av de mest populære videregående skolene i Oslo med elever fra alle kanter av byen. Skolen tilbyr flere linjer, blant annet; medier og kommunikasjon, elektrofag, design, håndverk og studiespesialisering med alt fra fysikk til kinesisk.\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_A-20141_Ua_0004_040jpg.jpg', 'text': '\xd8stre Elvebakke 1926', 'alttext': 'Oslo Yrkesskole (Ingressbilde)', 'width': 176, 'height': 242}, 'title': 'Oslo yrkesskole', 'url': 'http://www.industrimuseum.no/oslo-yrkesskole', 'coords': '59.91913468087322, 10.754027366638183', 'audio': ['http://www.industrimuseum.no/filearchive/08_Spor_7_Elveba.mp3']}, {'description': 'Her nede ved Ankerbroen ligger Ankertorvets Sekundærstasjon, som omformet strømmen fra kraftverket på Hammeren og sendte det ut til gatelys og trikk. Ved Hammeren i Maridalsvannet, kilden til Akerselva, ligger et av Norges første vannkraftverk. Dette var fra år 1900 hovedleverandør av energi til Hovedstaden. \r\n\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Oslo%20lysverkers%20transformatorstasjon%20ved%20Akerselva%20ved%20Elvebakkene1973%2C%20NTM.jpg', 'text': 'Foto 1973, NTM', 'alttext': 'kraft (Ingressbilde)', 'width': 238, 'height': 160}, 'title': 'Lysverkenes sekund\xe6rstasjon ', 'url': 'http://www.industrimuseum.no/kraftproduksjon-langs-elva', 'coords': '59.91914005839831, 10.754735469818115', 'audio': ['http://www.industrimuseum.no/filearchive/101_Lysverkets_sekund_rstasjon__Duc_.mp3']}, {'description': 'Ankerbrua har navn etter Ankerløkka og Ankertorget, som lå der Anker hotell ligger i dag. Brua er en forlengelse av Torggata ut av sentrum. På den andre siden av elva møter den krysset mellom Søndre gate og Markveien, som er begynnelsen på nedre GrÅnerløkka. Den første brua her ble anlagt i 1874. Det var da en trebru. Dagens bru fra 1926 er i granitt.\r\nI I937 fikk brua fire bronseskulpturer av Dyre Vaa, en i hvert hjørne. Disse har eventyrmotiv hentet fra Asbjørnsen og Moe, og Ankerbrua kalles derfor Eventyrbrua i blant. Motivene er Kvitebjørn Kong Valemon, Peer Gynt, Kari Trestakk og Veslefrikk med fela. I forbindelse med planene om sanering av GrÅnerløkka på 1960-tallet var Ankerbrua et av få byggverk som var tenkt bevart. \r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20051.jpg', 'text': 'Eventyrbrua, 2011', 'alttext': 'akerselvavandring2011_dagandreassen 051.jpg', 'width': 237, 'height': 178}, 'title': 'Ankerbrua ei eventyrbr', 'url': 'http://www.industrimuseum.no/86_ankerbrua', 'coords': '59.918252754838924, 10.756248235702514', 'audio': ['http://www.industrimuseum.no/filearchive/102_Ankerbrua_-_ei_eventyrbru__Duc_.mp3']}, {'description': 'Langs Akerselva, nåværende Hausmanns gate, lå tett med små løkker som på 1700-tallet tilhørte general Hausmann i Mangelsgården, Storgata 36. I 1750-årene ble eiendommen solgt til kjøpmann Christian Ancher og fikk navn etter ham. Sønnen Peder Anker på Bogstad gård solgte arealet nord for Hausmanns gate til Christiania kommune i 1823. I 1830 tok kommunen husene på løkka i bruk til skole for Fjerdingen, fattigstrøket langs Christian Krohgs gate. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_oslobyarkiv_kart_elvebakken1860.jpg', 'text': '', 'alttext': 'Kartutsnitt 1860', 'width': 237, 'height': 214}, 'title': 'Ankerløkka', 'url': 'http://www.industrimuseum.no/89_ankerlokken', 'coords': '59.91755365047812, 10.75836181640625', 'audio': ['http://www.industrimuseum.no/filearchive/106_Ankerl_kka.mp3']}, {'description': 'I 1833 ble byen rammet av en voldsom kolera-epidemi. Byen hadde ikke sykehusplass til alle som ble syke, og som nødløsning ble skolen på Ankerløkka tatt i brukt til lasarett. Sykdommen var svært dødelig, og de mange dødsfallene gjorde at byen også slapp opp for gravplasser på kirkegården. I all hast ble Ankerløkkens Kirkegaard anlagt – beliggenheten ved kolerasykehuset var praktisk, siden det var kort vei å frakte likene. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Kart%20over%20Ankerl.png', 'text': '', 'alttext': 'Ankerlokken gravlund (Ingressbilde)', 'width': 238, 'height': 172}, 'title': 'Kolerakirkegård og epidemilasarett', 'url': 'http://www.industrimuseum.no/91_kolerakirkegard', 'coords': '59.917408450033264, 10.756183862686157', 'audio': ['http://www.industrimuseum.no/filearchive/108_Kolerakirkegaard_og_epidemilasarett__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/109_Kolerakirkegaard_og_epidemilasarett-1..mp3']}, {'description': 'Hvem var Maren i myra?\r\n\r\nIngen vet hvem hun var og hva hun egentlig het, men du kan \r\nse henne på Teknisk Museum - der hun ligger utstilt i en monter.\r\n\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Maren%20i%20Myra.jpg', 'text': 'Her er et bilde av "Maren i myra" utstilt på Norsk Teknisk Museum.', 'alttext': '"Maren i myra" (Ingressbilde)', 'width': 237, 'height': 178}, 'title': '"Maren i myra"', 'url': 'http://www.industrimuseum.no/Maren_i_myra', 'coords': '59.91887252543008, 10.755314826965332', 'audio': ['http://www.industrimuseum.no/filearchive/04_Spor_3__Maren.mp3']}, {'description': 'Ankerløkken - området fra Jacobs kirke til Nybrua og Storgata hadde vært kolerakirkegård. Da kirkegården ble nedlagt tok Oslo kommune området i bruk til bl.a. nye Jakobs kirke, gassverket, øvelsesplass for brannvesenet, høytorg, kjøttkontrollstasjon og elektrisitetsverk. Statens kirkemyndighet, Stiftsdireksjonen, protesterte: Den ville anlegge en park rundt kirken, og mente staten eide området. Byen gikk seirende ut av den bitre kampen og beholdt Ankerløkka til sine mange nyttige formål.\r\n', 'img': {}, 'title': 'Ankerløkka - høytorg og busstasjon', 'url': 'http://www.industrimuseum.no/93_ankerlokka_buss', 'coords': '59.91732778284511, 10.757911205291748', 'audio': ['http://www.industrimuseum.no/filearchive/111_Ankerl_kka_-_h_ytorg_og_busstasjon__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/112_Ankerl_kka_-_h_ytorg_og_busstasjon.mp3']}, {'description': 'Da hestetransport var den viktigste kommunikasjonen var lett tilgjengelig hestef\xf4r også midt i byene av stor betydning. Et høytorg hvor dyref\xf4r ble fraktet til og solgt var selvsagt i alle byer. I Oslo lå Høytorget på Ankerløkka i enden av Storgata. I en reportasje i Arbeiderbladet 5. januar 1955 møter vi den siste høykrøreren på Høytorget før den nye tids transport overtok området: Busstasjon for Oslo.\r\n', 'img': {}, 'title': 'Den siste høykjøreren på Ankertorget', 'url': 'http://www.industrimuseum.no/94_siste_hoy_ankertorget', 'coords': '59.916757729126196, 10.757439136505127', 'audio': ['http://www.industrimuseum.no/filearchive/113_Den_siste_h_ykj_reren.mp3', 'http://www.industrimuseum.no/filearchive/114_Den_siste_h_ykj_reren__Duc_.mp3']}, {'description': 'Få kjenner historien til det lille rødmalte trehuset som ligger på et hjørne av Ankertorget ved Ankerbrua. Nå holder en barnehage til i huset, men fra 1928 til 1950-årene var dette Østkantutstillingen, et tiltak for å lære arbeiderklassen bedre boligkultur. Initiativtaker til dette prosjektet var Nanna Broch, Oslo kommunes boliginspektør. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_OSdf-113684_84286.jpg', 'text': '\xd8stkantutstillingen, Riksarkivet', 'alttext': 'Ostkantut (Ingressbilde)', 'width': 237, 'height': 168}, 'title': '\xd8stkantutstillingen', 'url': 'http://www.industrimuseum.no/Ostkantutstillingen', 'coords': '59.91795698171484, 10.75562596321106', 'audio': ['http://www.industrimuseum.no/filearchive/05_Spor_4__stkan.mp3']}, {'description': 'Et landemerke ved Nybrua der GrÅnerløkka starter er Schous bryggeri. Dette var et av byens ledende bryggerier, startet i 1821 av Christian Schou. Det lå først ved Fjerdingen i Chr. Kroghs gate, fra 1873 i Trondheimsveien 2. Bryggeriet ble fusjonert med Frydenlund i 1962, oppkjøpt av Nora i 1978, og nedlagt i 1982. Anlegget omdisponert til bl.a. Undervisningslokaler for BI. I dag rommer Schou-kvartalet bl.a. Oslo kommunes Kulturskole og Pop-senteret.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Schous_Bryggeri.jpg', 'text': 'Foto: Håkon Bergseth/Norsk Teknisk Museum', 'alttext': 'Schous (Ingressbilde)', 'width': 190, 'height': 243}, 'title': 'Schous Bryggeri', 'url': 'http://www.industrimuseum.no/bedrifter/schousbryggeri', 'coords': '59.91828502083848, 10.759831666946411', 'audio': ['http://www.industrimuseum.no/filearchive/103_Schous_bryggeri.mp3']}, {'description': 'Til 1859 var Akerselva grense mellom byen og landet, mellom Christiania og Aker. Det året ble grensen skjøvet lenger østover på Akers bekostning, men det var bøndene i Aker glade for. Det var de som hadde ivret for byutvidelse for å slippe sosiale utgifter til den voksende befolkningen langs elva. I 1837 ble det kommunalt selvstyre i landet gjennom formannskapslovene. Da ble også Aker et selvstyrt herred med et formannskap valgt av innbyggere med stemmerett. De fleste var bønder, og ingen var kvinner. Aker trengte også et eget herredshus, og det ble det råd til etter 1850. Herredet valgte å reise sin administrasjonsbygning så nær bygrensen som mulig, for kontakten med byen og statsmyndighetene var viktig. Tomten fant de ved Nybrua fra 1827, som var blitt hovedinnfartsveien til byen og forbandt Trondhjemsveien med Storgata. Herredshuset på Trondheimsveien 3 var et samarbeidsprosjekt mellom Sparebanken og kommunen, og det sto ferdig i 1853. Sparebanken var byggherre, og kommunen leide lokaler for herredsstyret og kontorplass for ordfører, kasserer og andre tjenestemenn. De første årene lå herredshuset i Aker, men fra 1859, lå det i Christiania. Og der ble det liggende – og enda mer sentralt i byen etter nok en byutvidelse i 1878, helt til hele hele Aker ble en del av Oslo i 1948.', 'img': {}, 'title': 'Elva som kommunegrense', 'url': 'http://www.industrimuseum.no/88_kommunegrense', 'coords': '59.918231244155066, 10.76087236404419', 'audio': ['http://www.industrimuseum.no/filearchive/104_Elva_som_kommunegrense.mp3', 'http://www.industrimuseum.no/filearchive/105_Elva_som_kommunegrense__Duc_.mp3']}, {'description': 'Nybrua ble anlagt av Karl Johan i 1827. Han ønsket en mer standsmessig innfart til byen enn det Vaterland lenger nede kunne tilby. Strøket fikk derfor et småborgerlig preg, og ble mål for byborgernes søndags-promenader. Her ble byens første offentlige park anlagt, og det første offentlige monument, nemlig Krohgstøtten. Men fattigdommen var aldri langt unna. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20049.jpg', 'text': '', 'alttext': 'Nybrua med trikk i Storgata / Trondheimsveien', 'width': 237, 'height': 178}, 'title': 'Nybrua', 'url': 'http://www.industrimuseum.no/96nybrua', 'coords': '59.91770951228193, 10.759177492065426', 'audio': ['http://www.industrimuseum.no/filearchive/115_Nybrua__Duc_.mp3']}, {'description': 'Kjenningsmelodi for Fjerdingen. Med utgangspunkt i Akerselvasangen og Schous Bryggeri.', 'img': {}, 'title': 'Akerselva Remix - Fjerdingen', 'url': 'http://www.industrimuseum.no/97_akerselva_remix_kjenning_fjerdingen', 'coords': '59.915838, 10.758920', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%2010.mp3']},  {'description': 'Minnestøtten over Christian Krohg ved Nybrua er byens første offentlige monument. Støtten ble reist i 1833 til minne om en av heltene fra 1814, stortingspresident Christian Krohg. Det er en obelisk av støpejern laget ved Nes jernverk etter tegning av tidens store arkitekt Christian H. Grosch. Den står på en bred sokkel, og på vestsiden ligger en sørgmodig norsk løve med sin øks mellom labbene. På sokkelen står innskriften: Fædrelandets varme Talsmand, Norges trofaste Søn, Sandhedens beskjedne Tolk.', 'img': {}, 'title': 'Kroghstøtten - byens første monument', 'url': 'http://www.industrimuseum.no/98_kroghstotten', 'coords': '59.91730896047281, 10.759311318397522', 'audio': ['http://www.industrimuseum.no/filearchive/118_Krohgst_tten_-_byens_f_rste_monument__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/118_Krohgst_tten_-_byens_f_rste_monument__Duc_-1..mp3']}, {'description': 'For hundre år siden var Kristiania-industrien på vei til å tape terreng i forhold til annen industri som lå nær jernbane og havn. "Gjør Akerselven farbar for pramme", lød det i et fellesskriv fra samtlige brukseiere til kommunen i 1908. Løsningen, mente mange, var å kanalisere Akerselva slik at lossefartøyene kunne seile oppover elva og losse lasten direkte inn i fabrikkene og verkstedene. Direktøren ved Vulkan, hevdet at “ Bare ved å mudre opp elva og gjøre den farbar for grundtgående prammer, kunne problemet løses. Trafikken på Kristianias gater ville bli lettere, ny industri ville lokkes til hovedstaden og den allerede ”nu stærkt optagne havn” ville få avlastning fordi "mange Fartøier kunde ligge paa Havnen og losse lige i Pramme". ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-047.jpg', 'text': 'Sam Eydes pakkhus A', 'alttext': 'Sam Eydes pakkhus A i Christian Kroghs gate, Fjerdingen', 'width': 176, 'height': 242}, 'title': 'Gjør Akerselva farbar for prammene', 'url': 'http://www.industrimuseum.no/gjor-akerselva-farbar-for-prammene', 'coords': '59.91573053214448, 10.759928226470947', 'audio': ['http://www.industrimuseum.no/filearchive/25_Spor_24_Pramm.mp3', 'http://www.industrimuseum.no/filearchive/117_Kanal_og_indre_havn__Duc_-1..mp3']}, {'description': 'Mellom Vaterland og Fjerdingen passerer man Hausmanns bro som tar Hausmannsgata over Akerselva. Den hvite støpejernsbrua fra 1892 ble bygget dønn solid for at den skulle kunne bære kommunens nye dampveivals, veiende 18 tonn. I 1986 ble broen utvidet til 6 felts kjørevei, men \xabjernblondene\xbb ble bevart for ettertiden.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20038.jpg', 'text': '', 'alttext': 'Hausmanns Br', 'width': 237, 'height': 178}, 'title': 'Hausmanns br', 'url': 'http://www.industrimuseum.no/102_hausmannsbr', 'coords': '59.91520347632608, 10.759906768798828', 'audio': ['http://www.industrimuseum.no/filearchive/123_Hausmanns_bru__Duc_.mp3']}, {'description': 'Den Norske Remfabrikk var en av de mange fabrikkene som vokste seg store på å levere utstyr til industrien. Industrialiseringen forsterket seg selv på den måten. Reimer og akslinger var sentralt for overføring av kraft fra turbiner, stasjonære dampmaskiner og senere elektromotorer direkte til arbeidsmaskiner i industrien og verksteder. Gjennom store industribygg gikk det intrikate systemer av akslinger, gir og veksler som alt ble bundet sammen av reimer av lær, gummi eller senere syntetiske materialer. Da prisene og størelsene på elektormotorer sank drastisk etter 1950, fikk hver maskin sin egen motor. Behovet for reimer forsvant nesten helt, og de mange reimfabrikkene ble lagt ned. Den Norske Remfabrikk fant imidlertid markeder for sine spesialprodukter i mange tiår til.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_akerselva%20nedre%20chr%20krohg%202%20norsk%20remfabrikk%20039.jpg', 'text': '', 'alttext': 'akerselva nedre chr krohg 2 norsk remfabrikk 039.jpg', 'width': 238, 'height': 185}, 'title': 'Den Norske Remfabrik A/S', 'url': 'http://www.industrimuseum.no/bedrifter/dennorskeremfabr_a_s', 'coords': '59.9141938, 10.7575228', 'audio': ['http://www.industrimuseum.no/filearchive/120_Remfabrikken.mp3', 'http://www.industrimuseum.no/filearchive/121_Remfabrikken__Duc_.mp3']}, {'description': 'Abelone ble født på Tukthuset i Storgata en gang på 1850-tallet. Moren hadde havnet der som resultat av løsgjengerloven. Hun tjente til livets opphold ved å trekke på gata, noe lovens lange arm ikke tillot. Abelones liv ble tidlig skilt fra morens, og en snill familie på Sinsen lot henne vokse opp hos dem. Men så fort hun hadde stått til konfirmasjon, stakk hun hjemmefra. Hun ble gjenfunnet på et bordell i Vaterland, der hun traff sin tilkommende. Sammen bygget de opp en delikatesseforretning i strøket, med værelser ovenpå der man kunne få kjøpt fluidium og dertil tilgang på piker. \r\n', 'img': {}, 'title': 'Abelone \u2013 Vaterlands dronning ', 'url': 'http://www.industrimuseum.no/104_abelone', 'coords': '59.913299551920716, 10.756301879882812', 'audio': ['http://www.industrimuseum.no/filearchive/126_Abelone.mp3']}, {'description': 'I denne fortellingen fra Aftenposten 11.september 1948 får vi høre om et ungt par fra Oslos vestkant som padler ut fra Skarpsno i kano en tidlig høstnatt i 1948 og våger seg opp i skumle strøk langs Akerselvas nedre del. De passerer fjordrestaurantene, Vippetangen, og tar elveleiet oppover. Passerer Bispebroa, Schweigaards bru (ingen av disse eksisterer lenger) padler under Vaterlands bru og nesten opp til Nybrua:', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_OBA3726jpg.jpg', 'text': '', 'alttext': 'Akerselva pram 1', 'width': 238, 'height': 175}, 'title': 'Med kano fra Skarpsno til Nybrua en lørdagsnatt', 'url': 'http://www.industrimuseum.no/106_kano_tekst', 'coords': '59.91393958389123, 10.75859785079956', 'audio': ['http://www.industrimuseum.no/filearchive/130_Med_kano_fra_Skarpsno_til_Nybrua_en_l_rdagsnatt__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/129_Med_kano_fra_Skarpsno_til_Nybrua_en_l_rdagsnatt.mp3']}, {'description': 'Helt fra middelalderen ble korn fraktet oppover Akerselva til kornmølla på Nedre Foss. Hva slags båter ble brukt? Arkeologer fant svar midt i det som i dag er bussterminalen på Grønland! Og ved å se på gamle Oslobilder. 9. Februar 2011 ble det funnet rester etter et lite båtvrak i Schweigaardsgate like utenfor bussterminalen i Oslo. Det var kun rester etter bunnskroget som lå igjen, men vraket som var 5 meter langt og 2 meter bredt kunne gi et godt inntrykk av hvordan båten opprinnelig kan ha sett ut. Årringsanalyser av treverket i båten viste av treverket var hugget en gang mellom 1502 og 1505. Båten har trolig ligget like ved Akerselvas datidige utløp i fjorden ved Vaterland.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Vaterland1g.jpg', 'text': 'Arkeolog Solveig Thorkildsen fra Byantikvaren fant båtvraket. MArinarkeologer fra Norsk Maritimt museum bisto utgravningen og forvalter funnet. Båten var ferdig avdekt 8. mars 2011. Fv. Feltleder Lotte Carrasco, Kristian Løseth, Andreas Kerr (alle NMM). Foto: Tori Falck/NMM. ', 'alttext': 'Båtfunn på Vaterland (Ingressbilde)', 'width': 237, 'height': 178}, 'title': 'En båt under bussterminalen', 'url': 'http://www.industrimuseum.no/105_vaterland_arkeologi', 'coords': '59.9115461238152, 10.75887680053711', 'audio': ['http://www.industrimuseum.no/filearchive/127_En_b_t_under_bussterminalen__Duc_.mp3', 'http://www.industrimuseum.no/filearchive/128_En_b_t_under_bussterminalen.mp3']}, {'description': 'Ved utløpet av Akerselva, der den nye Operaen nå ligger, lå et av landets betydeligste skipsverft. Den enorme ekspansjonen i handelsflåten etter 1850, og omleggingen til stål og damp gjorde Nyland til et ledende skipsverft.', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Nyland-1..jpg', 'text': 'Nyland Verksted, Foto: NTM', 'alttext': 'Nyland1 (Ingressbilde)', 'width': 237, 'height': 139}, 'title': 'Nylands verksted', 'url': 'http://www.industrimuseum.no/bedrifter/nylandsverksted', 'coords': '59.90635287127178, 10.753512643041958', 'audio': ['http://www.industrimuseum.no/filearchive/132_350_skip_av_st_l__Duc_.mp3']}, {'description': 'I en sjelden utereportasje i NRK Radio 16. september 1936 males et bilde av en travel havn i hovedstaden. Et av de siste stoppene gjøres i Bjørvika, der Nyland verksteds direktør Sigurd Pauss intervjues. Han forteller om det som den gang var en av byens viktigste industribedrifter, og som hadde ordrebøkene fulle. ', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Nyland-1..jpg', 'text': '', 'alttext': 'Nyland1 (Ingressbilde)', 'width': 237, 'height': 139}, 'title': 'Mikrofonbesøk på Oslo havn i 1936', 'url': 'http://www.industrimuseum.no/NRKhavna', 'coords': '59.90666185422423, 10.754456520080566', 'audio': ['http://www.industrimuseum.no/filearchive/nrk%20havna%201936%20nyland.mp3']}, {'description': 'Hans Arnt Hartvig Paulsen drev sin kullforretning fra området på østsiden av Akerselvas utløp i Bjørvika som i dag bærer navnet Paulsenkaia. Paulsen hadde vært til sjøs som ung, ble skipsoffiser og fikk gode handelsforbindelser i England og gode inntekter som han benyttet til å slå seg opp i Oslo. Han investerte i eiendom og ble Oslos ledende kullhandler. Han gikk for å være en særpreget personlighet som ga opphav til mange historier og skrøner hvor "Køla-Pålsen" var hoverperson som helt eller anti-helt. "Køla-Pålsen" havnet ofte i mange pussige og pinlige situasjoner som han enten havnet i eller kom seg ut av på grunn av sine grovinnstilte sosiale antenner og skarpe replikk. At mange av historiene om Køla-Pålsen handler om episoder med nazistene og andre verdenskrig viser at historiene ikke nødvendigvis er sanne. Paulsen døde i 1914... ', 'img': {}, 'title': 'Køapålsen', 'url': 'http://www.industrimuseum.no/110_paulsen', 'coords': '59.9066726133169, 10.755486488342285', 'audio': ['http://www.industrimuseum.no/filearchive/133_K_lap_lsen.mp3']},{'description': 'Vignett for Kjelsås - Brekke.', 'img': {}, 'title': 'Akerselva remix - Kjelsås', 'url': 'http://www.industrimuseum.no/02vignett1_brekke', 'coords': '59.967013864281924, 10.779304504394531', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%201.mp3']},{'description': 'Kjenningsmelodi for Nydalen. Med utgangspunkt i Akerselvasangen og lyden fra spadeproduksjonen i Fiskars-fabrikken, 2012. ', 'img': {}, 'title': 'Akerselva Remix - Nydalen', 'url': 'http://www.industrimuseum.no/10nydalen_kjenning', 'coords': '59.951346551845134, 10.763490200042724', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%202.mp3']},{'description': 'Kjenningsmelodi for Vøyenfallene - hjertet av Akerselva. Med utgangspunkt i Akerselvasangen og lyden tekstilmaskiner. ', 'img': {}, 'title': 'Akerselva Remix - Vøyenfallene', 'url': 'http://www.industrimuseum.no/33_akerselva_remix_kjenning_voyen', 'coords': '59.93129104084156, 10.757997035980224', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%206.mp3']},{'description': 'Akerselva Digitalt: Kjenningsmelodi for Lilleborg. Med utgangspunkt i Akerselvasangen og reklame for Lilleborg-såpe.\r\n.\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_9731a1f9c5.jpg', 'text': 'Bilde1', 'alttext': 'Lilleborg', 'width': 181, 'height': 242}, 'title': 'Akerselva Remix - Lilleborg', 'url': 'http://www.industrimuseum.no/24_akerselva_remix-lilleborg', 'coords': '59.93784857144112, 10.762288570404052', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%204.mp3']},{'description': 'Akerselva Digitalt: Kjenningsmelodi for Bjølsen og Akerselvas høyeste foss. Med utgangspunkt i Akerselvasangen og lyden fra kornmalingen i Bjølsen valsemølle.\r\n', 'img': {'url': 'http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-083.jpg', 'text': '', 'alttext': 'Bjøsen Valsemøle', 'width': 237, 'height': 178}, 'title': 'Akerselva Remix - Bjøsen', 'url': 'http://www.industrimuseum.no/18_akerselva_remix_bjolsen', 'coords': '59.940804411259066, 10.767019987106323', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%203.mp3']},{'description': 'Kjenningsmelodi for Elvebakken / Ny York. Med utgangspunkt i Akerselvasangen og jazzklubben blå, tidligere lysbære og neonlysfabrikk, og dagens sveiseverksted i området.\r\n', 'img': {}, 'title': 'Akerselva Remix - Elvebakken', 'url': 'http://www.industrimuseum.no/73_akerselva_remix_kjenning_indigo', 'coords': '59.91916156849296, 10.755336284637451', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%209.mp3']},{'description': 'Kjenningsmelodi for Vaterland. Med utgangspunkt i Akerselvasangen og båtlivet som tildigere preget nedre del av Akerlselva.', 'img': {}, 'title': 'Akerselva Remix - Vaterland', 'url': 'http://www.industrimuseum.no/103_akerselva_remix_kjenning_vaterland', 'coords': '59.913826637969834, 10.756773948669433', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%2012-1..mp3']},{'description': 'Kjenningsmelodi for Bjørvika. Med utgangspunkt i Akerselvasangen Den norske opera og ballett.', 'img': {}, 'title': 'Akerselva Remix - Bjørvika', 'url': 'http://www.industrimuseum.no/107_akerselva_remix_kjenning_bjorvika', 'coords': '59.907565605855595, 10.755465030670166', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%2013.mp3']},{'description': 'Kjenningsmelodi for Øvre Foss. Med utgangspunkt i Akerselvasangen og kunsthøyskolene i den gamle Seilduksfabrikken.', 'img': {}, 'title': 'Akerselva Remix - \xd8vre Foss', 'url': 'http://www.industrimuseum.no/56_akerselva_remix_kjenning_foss', 'coords': '59.92678601865637, 10.753684043884277', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%207.mp3']},{'description': 'Kjenningsmelodi for Nedre Foss / Vulkan. Med utgangspunkt i Akerselvasangen, Bellona, treningssenter og annet... ', 'img': {}, 'title': 'Akerselva Remix - Vulkan', 'url': 'http://www.industrimuseum.no/66_akerselva_remix_vulkan', 'coords': '59.92233684329643, 10.75335144996643', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%208.mp3']},{'description': 'Kjenningsmelodi for Myren. Med utgangspunkt i Akerselvasangen og lyden fra Hotell Cæsar, TV-serie produsert på Myren.', 'img': {}, 'title': 'Akerselva Remix - Myren', 'url': 'http://www.industrimuseum.no/28_akerselva_remix_myren', 'coords': '59.93426359036041, 10.758233070373535', 'audio': ['http://www.industrimuseum.no/filearchive/Nr%205.mp3']}];


// The zones
// 0: NYDALEN
// 1: KJELSÅS
// 2: VØYEN
// 3: Kjelsås Mustad delsone
// 4: LILLEBORG
// 5: BJØLSEN
// 6: ELVEBAKKEN
// 7: FJERDINGEN
// 8: VATERLAND
// 9: BJØRVIKA
// 10: ØVRE FOSS
// 11: NEDRE FOSS
// 12: MYREN

var zones = [
             
    {
        title : "NYDALEN (sone)",
        id : "zone-0",
        description: "Nydalsbyen - boliger, hotell, T-bane, TV-studioer, teater, campus, kontorer, kafeer og restauranter. Langs dette stykket av Akerselva kan vi kanskje se de største forandringene de siste tiårene. Fra 1840-årene vokste dette stille jordbruksområdet i Aker til et typisk brukssamfunn utenfor Oslo, med store fabrikker omgitt av arbeiderboliger, skoler, forsamlingshus og et yrende foreningsliv. Hele dalen ble dekket av fabrikkbygninger med tusenvis av arbeidere som produserte myke tekstiler og hardt metall. Mange av de røde mursteinsbygningene er bevart mellom alle nybyggene som erstattet industrien etter 1989.",
        src: ["http://www.industrimuseum.no/filearchive/013_Fra_bruksby_p__landet_til_Nydalsbyen.mp3", "http://www.industrimuseum.no/filearchive/014_Fra_bruksby_p__landet_til_Nydalsbyen__Duc_.mp3"],
        zoneimg: "img/sone-00.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20140.jpg",
        url: "http://www.industrimuseum.no/11nydalen_ingtro",
        zonepoint: [59.951529,10.765311],
        lat : 59.951529,
        lng : 10.765311,
        vertices :
        [
            { "y" : 10.766602, "x" : 59.953807},
            { "y" : 10.768061, "x" : 59.952904},
            { "y" : 10.769520, "x" : 59.952754},
            { "y" : 10.770464, "x" : 59.952217},
            { "y" : 10.773125, "x" : 59.947489},
            { "y" : 10.765829, "x" : 59.946199},
            { "y" : 10.763083, "x" : 59.946586},
            { "y" : 10.759735, "x" : 59.945856},
            { "y" : 10.759993, "x" : 59.948714},
            { "y" : 10.760722, "x" : 59.950369},
            { "y" : 10.760508, "x" : 59.953936},
            { "y" : 10.761666, "x" : 59.955397},
            { "y" : 10.763855, "x" : 59.956579}, 
            { "y" : 10.766687, "x" : 59.957546}, 
            { "y" : 10.766602, "x" : 59.953807}
        ]
    },
    
    {
        title: "KJELSÅS/BREKKE (sone)",
        id : "zone-1",
        description: "På Kjelsås starter Akerselvas ferd. Ved Maridalsvannet og rundt Brekkedammen var det sagbruk og møller i mange hundre år. Eventyrforteller Asbjørnsen har beskrevet området i sitt Kvernsagn.   Med industrialiseringen rundt 1850 kom også verkstedindustri til Kjelsås med Kjelsås Brug. Hundre år senere var det moderne industri som Tandberg som fant veien til store ledige arealer av byen som ble regulert til industriformål.  I dag er det fortsatt noen industri- og kontorbygg her, men mange er omgjort til boliger i det som er et av de mest populære boområdene i Oslo.",
        src: ["http://www.industrimuseum.no/filearchive/003_Kjels_s.mp3", "http://www.industrimuseum.no/filearchive/Nr%201.mp3"],
        zoneimg: "img/sone-01.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_luftfoto_kjelsas_tekniskmuseum_zovenfra2012_3.jpg",
        url: "http://www.industrimuseum.no/01kjelsas_artikkel",
        zonepoint: [59.967384,10.776554],
	lat : 59.967384,
	lng : 10.776554,
        vertices :
        [
            { "y" : 10.789347, "x" : 59.969554},
            { "y" : 10.789089, "x" : 59.967234},
            { "y" : 10.785441, "x" : 59.965065},
            { "y" : 10.783553, "x" : 59.962852},
            { "y" : 10.780549, "x" : 59.963754},
            { "y" : 10.777974, "x" : 59.964356},
            { "y" : 10.775957, "x" : 59.963797},
            { "y" : 10.774198, "x" : 59.962702},
            { "y" : 10.771923, "x" : 59.960768},
            { "y" : 10.770807, "x" : 59.960081},
            { "y" : 10.766773, "x" : 59.961026},
            { "y" : 10.768833, "x" : 59.964162},
            { "y" : 10.767975, "x" : 59.967556},
            { "y" : 10.770807, "x" : 59.969618},
            { "y" : 10.773382, "x" : 59.970391},
            { "y" : 10.782566, "x" : 59.970048},
            { "y" : 10.785441, "x" : 59.969403},
            { "y" : 10.787802, "x" : 59.96936},
            { "y" : 10.789347, "x" : 59.969554}
        ]
    },
    
    {
        title: "VØYEN (sone)",
        id : "zone-2",
        description: "Vøyen er på mange vis selve hjertet i Akerselva. Her er vi midt på elvestrekningen mellom Kjelsås og Bjørvika. De industrihistoriske sporene ligger tett langs de flotte Vøyenfallene som fører Akerlselva 20 meter ned fra Myren gjennom flere fall og terskler som sykkker fossen opp med navn som Hjulafossen, Nedre Vøyen, Våghalsen og Labakken. Her var det sager og møller, og her reiste de aller første industribygg av tre seg på 1700-tallet. De mektige tekstilindustribygningene reiste seg ved fossen da den engelske industrielle revolusjonen virkelig spredde seg etter 1840. Hjulafossen og Vøyen ga navn til de største tekstilfabrikkene som ble bygd her i 1846 og 1855. Ved Vøyenbrua lå byens første drikkevannsinntak. Her er vi også midt i Oskar Bråtens litterære univers. Rundt Beierbrua drives kulturvirksomhet bl.a. i 'Hønse-Lovisas Hus' og formidling av industri- og arbeiderhistorie i Oslo Museums besøksleilighet og senter for den nye museumssatsingen rundt Akerlselva i regi av Oslo Museum og Teknisk museum. .",
        src: ["http://www.industrimuseum.no/filearchive/048_V_yen_-_hjertet_midt_i_Akerselva.mp3", "http://www.industrimuseum.no/filearchive/049_V_yen_-_hjertet_midt_i_Akerselva__Duc_.mp3"],
        zoneimg: "img/sone-02.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_voyenfoss_luft_utsnitt2011.jpg",
        url: "http://www.industrimuseum.no/39voyen_intro_tekst",
        zonepoint: [59.930689,10.755311],  //vøyensvingen
        lat : 59.930689,
	    lng : 10.755311,
        vertices:
        [
            { "y" :  10.756109, "x" : 59.93343},
            { "y" :  10.758469, "x" : 59.932742},
            { "y" :  10.758598, "x" : 59.932495},
            { "y" :  10.758598, "x" : 59.93171},
            { "y" :  10.759714, "x" : 59.931528},
            { "y" :  10.761194, "x" : 59.930936},
            { "y" :  10.760894, "x" : 59.929367},
            { "y" :  10.760894, "x" : 59.92856},
            { "y" :  10.758984, "x" : 59.92841},
            { "y" :  10.756860, "x" : 59.928356},
            { "y" :  10.752933, "x" : 59.928302},
            { "y" :  10.753298, "x" : 59.929571},
            { "y" :  10.754070, "x" : 59.931098},
            { "y" :  10.754564, "x" : 59.931936},
            { "y" :  10.756109, "x" : 59.93343}
        ]
    },
     
    {
        title : "LILLEBORG (sone)",
        id : "zone-4",
        description: "Ved Akerselva mellom Bjølsen og Bentse Brug, hadde det vært stor virksomhet også på 1700-tallet. På plassen Jerusalem ble det bygd papirmølle. Treschow hadde startet det første såpekokeriet og linoljemølle.I 1811 ble området hetende Lilleborg. Papir- og oljeproduksjonen fortsatte, og det kom til noe tekstilproduksjon. Peter Møller startet tranproduksjonen sin her. Men Lilleborg skulle bli mer kjent som såpe enn som et sted i Oslo. Industrimannen Peter Kildahl overtok anleggene i 1863, og satset fra da for fullt på såpefabrikken. I tillegg ble oljemøllen videreført for å gi råstoff både til såpe og til maling- og lakkindustrien.",
        src: ["http://www.industrimuseum.no/filearchive/Nr%204.mp3"],
        zoneimg: "img/sone-04.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-073.jpg",
        url: "http://www.industrimuseum.no/25lilleborg_tekst",
        zonepoint: [59.9373097, 10.7657861],
        lat : 59.9373097,
        lng : 10.7657861,
        vertices :
        [
            { "y" : 10.765915, "x" : 59.939988},
            { "y" : 10.766816, "x" : 59.939052},
            { "y" : 10.768168, "x" : 59.938526},
            { "y" : 10.767417, "x" : 59.937408},
            { "y" : 10.764091, "x" : 59.936440},
            { "y" : 10.761645, "x" : 59.936516},
            { "y" : 10.760658, "x" : 59.937526},
            { "y" : 10.762417, "x" : 59.938547},
            { "y" : 10.763597, "x" : 59.939472},
            { "y" : 10.765915, "x" : 59.939988}
        ]
    },
    
    {
        title : "BJØLSEN (sone)",
        id : "zone-5",
        description: "Fra Nydalen renner Akerselva rolig gjennom de gamle Lillo-jordene før den kaster seg ut i sin høyeste foss: Bjølsenfossen med sine 17 meter. Her lå det tidligere både sagbruk og teglverk. Havnens Teglverk ble lagt ned i 1926, og i 1933 kjøpte kommunen tomten med tanke på park og boligbygging. Den mektige fossen ble tidlig brukt som kraft til sager, og området har lenge gitt oss vårt daglige brød. Her har det ligget møller i mange hundre år. På oversiden av fossen og Treschows gate lå Iduns Gjærfabrikk helt til 2007. På nedsiden ved fossen har Bjølsen Valsemølle dominert siden 1884. Her produseres fortsatt det meste av matmelet for Østlandet i det som i dag er Lantm\xe4nnen Ceralias Bjølsen-anlegg med merket Regal.",
        src: ["http://www.industrimuseum.no/filearchive/025_V_rt_daglige_br_d_fra_Bj_lsen.mp3"],
        zoneimg: "img/sone-05.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-083.jpg",
        url: "http://www.industrimuseum.no/19bjolsenintro_tekst",
        zonepoint: [59.9404063383322, 10.76912632004698],
        lat : 59.9404063383322,
        lng : 10.76912632004698,
        vertices :
        [
            { "y" : 10.766859, "x" : 59.942664},
            { "y" : 10.770421, "x" : 59.942277},
            { "y" : 10.770957, "x" : 59.940976},
            { "y" : 10.768919, "x" : 59.939031},
            { "y" : 10.767138, "x" : 59.938966},
            { "y" : 10.765829, "x" : 59.940041},
            { "y" : 10.765636, "x" : 59.941439},
            { "y" : 10.766859, "x" : 59.942664}
        ]
    },
    
    {
        title : "ELVEBAKKER, ANKER OG NY YORK (sone)",
        id : "zone-6",
        description: "Akerlseva slynger seg rolig mellom Ny York på østsiden opp mot GrÅnerløkka og Elvebakkene og Ankerløkka på vestbreddene. I dette området var det ikke kraft og tomtestørrelser til stor industri, men mye som har vokst seg stort og kjent andre steder har hatt sitt opphav her: Skulpturene i Frognerparken er støpt her, både Hjula Veveri og Vulkan hadde sin spede start her, aluminiumsgryter, lyspærer og neonlys ble først produsert her. Da elektrifiseringen av byen tok til for alvor var det hit vannkraften fra Maridalen ble sendt først. De første moderne bryggeriene lå her. I dag er det kunst- og kulturaktivitet som preger området.",
        src: ["http://www.industrimuseum.no/filearchive/098_Elvebakker__Anker_og_Ny_York.mp3"],
        zonepoint: [59.91943,10.756123],
        zoneimg: "img/sone-06.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvabilder_dagandreassen2011-12-002.JPG",
        url: "http://www.industrimuseum.no/74_elvebakken_intro_tekst",
        lat : 59.91943,
        lng : 10.756123,
        vertices :
        [
            { "y" : 10.751152, "x" : 59.921216},
            { "y" : 10.754478, "x" : 59.921065},
            { "y" : 10.754521, "x" : 59.920183},
            { "y" : 10.755894, "x" : 59.920151},
            { "y" : 10.761452, "x" : 59.919398},
            { "y" : 10.761409, "x" : 59.917656},
            { "y" : 10.759885, "x" : 59.91786},
            { "y" : 10.757203, "x" : 59.91615},
            { "y" : 10.755787, "x" : 59.916785},
            { "y" : 10.754435, "x" : 59.917527},
            { "y" : 10.751388, "x" : 59.919516},
            { "y" : 10.751152, "x" : 59.921216}
        ]
    },
    
    {
        title : "FJERDINGEN (sone)",
        id : "zone-7",
        description: "Nedenfor Nybrua langs vestre bredd finnes fortsatt gatestubber som tydelig bærer gamle navn. Et eksempel er Slåmotgangen, oppkalt etter Hans Olav Slaamodt som i begynnelsen av 1800-tallet hadde gartneri ned mot elven. Andre navn er Øvre og Nedre Vaskegang som antyder vaskeplasser ved elva fra gammelt av. Hovedgata er Christian Krohgs gate, som fram til 1890-åra het Fjerdinggata.\r\nFjerdingen utgjorde i likhet med Vaterland lenger nede, en forstad til Christiania, og strakk seg langs elva helt ned til Vaterlands bru. Strøket var blant de eldste forstedene, og karakteristisk med bebyggelse helt ned til elva. Dette var fattigstrøk, og ikke overraskende ble det anlagt hele to fattighus her i 1807. \r\nBlant industrielle minnesmerker står fortsatt Brødrene Ottesens Dampchokoladefabrikk i Nedre Vaskegang 2. Navnet er så vidt leselig i fasaden ut mot elva. Bygget er tegnet av arkitektene Ekman og Smith, som også tegnet det karakteristiske Indigo-bygget tilpasset svingen i elva litt lenger opp.",
        src: ["http://www.industrimuseum.no/filearchive/116_Fjerdingen_-_bydelen_som_ble_borte.mp3"],
        zoneimg: "img/sone-07.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-046.jpg",
        url: "http://www.industrimuseum.no/97fjerdingen_introtekst",
        zonepoint: [59.915837, 10.758919],
        lat : 59.915837,
        lng : 10.758919,
        vertices :
        [
            { "y" : 10.759327, "x" : 59.917753},
            { "y" : 10.761280, "x" : 59.917538},
            { "y" : 10.761924, "x" : 59.917140},
            { "y" : 10.761752, "x" : 59.916107},
            { "y" : 10.759349, "x" : 59.913267},
            { "y" : 10.754070, "x" : 59.914397},
            { "y" : 10.759327, "x" : 59.917753}
        ]
    },
    
    {
        title : "VATERLAND (sone)",
        id : "zone-8",
        description: "«De fleste har sikkert hørt eller lest en hel del om det beryktede Vaterland fra gamle dager». Slik lyder innledningen i «En politimanns erindringer – Trekk fra det mørkeste Kristiania» gjengitt i St.Hallvard i 1937. Andre beskrivelser forteller om «et pittoresk område med kronglete gater, snekker i elva og trehus». Sannheten ligger vel et sted midt i mellom, men kan være vanskelig for oss å finne svar på i dag. Det meste ble nemmelig jevnet med jorda i 1960-årene og erstattet av høyhus og trafikkmaskiner. To broer forsvant, Schweigaards bru og Bispebrua, og alle båtene som lå tett i tett er også borte. Småbåtene måtte flytte til Hovedøya. I dag utgjør Vaterlandsparken et pusterom i bydelen, men det er også her vi må si farvel til Akerselva før den forsvinner under en 500 m lang kulvert, og til slutt renner ut i fjorden ved Bjørvika.",
        src: ["http://www.industrimuseum.no/filearchive/124_Vaterland.mp3", "http://www.industrimuseum.no/filearchive/125_Vaterland__Duc_.mp3"],
        zoneimg: "img/sone-08.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20034.jpg",
        url: "http://www.industrimuseum.no/103vaterland_intro_tekst",
        zonepoint: [59.912374,10.756295],
        lat : 59.912374,
        lng : 10.756295,
        vertices :
        [
            { "y" : 10.754392, "x" : 59.914149},
            { "y" : 10.758898, "x" : 59.913343},
            { "y" : 10.758061, "x" : 59.910804},
            { "y" : 10.757139, "x" : 59.909610},
            { "y" : 10.754821, "x" : 59.910223},
            { "y" : 10.753491, "x" : 59.912202},
            { "y" : 10.754392, "x" : 59.914149}
        ]
    },
    
    {
        title : "BJØRVIKA (sone)",
        id : "zone-9",
        description: "Mellom Nyland og HAH! Den siste refrengstrofen i Akerselva-sangen beskriver stedet der elva renner ut i fjorden i Bjørvika. I dag finnes hverken Nyland eller HAH: utløpet av kulverten som leder elva under jernbanesporene på Oslo S finner vi rett bak Operaen - det nye landemerket fra 2008. På banken av sand og sagflis fra elva ble det på 1800-tallet bygd kaier og dokker for bygging og reparasjon av skip. Nylands Mekaniske Verksted var et ledende skipsverft. På østsiden finner vi Paulsen-kaia, som har navn etter byoriginal, kullimportør og forretningsmann Hans A H Paulsen - 'Køla-Pålsen'. Hans kulllager lå her.",
        src: ["http://www.industrimuseum.no/filearchive/131_Fra_Nyland_til_nytt_land_meed_opera_og_barcode.mp3"],
        zoneimg: "img/sone-09.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_akerselvavandring2011_dagandreassen%20022-1..jpg",
        url: "http://www.industrimuseum.no/108_intro_bjorvika",
        zonepoint: [59.907985,10.753334],
        lat : 59.907985,
        lng : 10.753334,
        vertices :
        [
            { "y" : 10.751774, "x" : 59.909588},
            { "y" : 10.757139, "x" : 59.908523},
            { "y" : 10.759284, "x" : 59.907899},
            { "y" : 10.755486, "x" : 59.904402},
            { "y" : 10.751388, "x" : 59.904779},
            { "y" : 10.7498, "x" : 59.907157},
            { "y" : 10.751774, "x" : 59.909588}
        ]
    },
    
    {
        title : "ØVRE FOSS (sone)",
        id : "zone-10",
        description: "Industrien kom på midten av 1800-tallet; drønn og bråk hørtes blant annet fra Christiania Seildugsfabrik ved Øvre Foss, en arbeidsplass for bortimot tusen mennesker. Bygningen dominerer fortsatt landskapet som en av de største industribygningene i Kristiania fra denne epoken. Vi vet at fabrikken vakte allmenn beundring, men sikkert også et visst ubehag for de som hadde arbeidsplassen sin her. Etter industriens nedleggelse, og i tråd med moderne byutvikling, har kultur- og utdanningsinstitusjoner overtatt fabrikkslokalene flere steder.",
        src: ["http://www.industrimuseum.no/filearchive/075_G_rdsbruk__sagbruk__seilduk-og_transformasjon__Duc_.mp3", "http://www.industrimuseum.no/filearchive/076_G_rdsbruk__sagbruk__seilduk-og_transformasjon.mp3"],
        zoneimg: "img/sone-10.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-043.jpg",
        url: "http://www.industrimuseum.no/57ovrefoss_intro_tekst",
        zonepoint: [59.926851,10.755376],
        lat : 59.926851,
        lng : 10.755376,
        vertices :
        [
            { "y" : 10.754328, "x" : 59.928238},
            { "y" : 10.755851, "x" : 59.928291},
            { "y" : 10.755765, "x" : 59.927732},
            { "y" : 10.756087, "x" : 59.925463},
            { "y" : 10.754972, "x" : 59.925399},
            { "y" : 10.755422, "x" : 59.924571},
            { "y" : 10.754306, "x" : 59.924377},
            { "y" : 10.753126, "x" : 59.923635},
            { "y" : 10.752311, "x" : 59.923560},
            { "y" : 10.750916, "x" : 59.923646},
            { "y" : 10.750058, "x" : 59.925646},
            { "y" : 10.749671, "x" : 59.928044},
            { "y" : 10.754328, "x" : 59.928238}
        ]
    },
    
    {
        title : "NEDRE FOSS VULKAN (sone)",  
        id : "zone-11",
        description: "Nedre Foss er som navnet sier den nederste fossen i Akerselva. Så langt kunne man ta seg med båt, og det gjorde man alt i middelalderen. Høvedøya kloster anla en kornmølle her, drevet av kraften fra fossen. Korn ble fraktet inn med båter, og mel ble fraktet ut. Etter reformasjonen ble gården krongods og mølla til Kongens mølle, med to vannhjul og åtte kvernsteiner i 1723. Familien GrÅner kjøpte eiendommen og bygde hovedhus og en flott hage på 1700-tallet. På 1800-tallet ble hovedhuset bygd om med murfasader forå se mer staslig ut. Bygningen står fortsatt. Mølleanleggene ble modernisert og utvidet på 1800-tallet. Bjølsen Valsemølle overtok i 1927 og drev anleggene til ca. 1980. Møllebygningene ble revet, og området regulert til park. På den andre siden av elva var det først Bakaas Brug som drev forskjellig virksomhet, alt fra badeanstalt til stolfabrikk. Rundt 1900 overtok det mekaniske verkstedet Vulkan. Etter 2000 er området utviklet til en ny bydel. Miljøorganisasjonen Bellona har fått et miljøvennlig signalbygg med ny bro over elva. Rundt dette profileres Vulkan som et 'varemerke' for en ny og miljøvennlig bydel med hotell, mathall, konsertscene, idrettshall, Dansens Hus, Westerdals kommunikasjonsskole og nye boliger.",
        src: ["http://www.industrimuseum.no/filearchive/090_Fra_klosterkorn_og_krongods_til_urbant_milj_fyrt_rn.mp3"],  
        zoneimg: "img/sone-11.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-055.jpg",
        url: "http://www.industrimuseum.no/67_nedrefoss_intro_tekst",
        zonepoint: [59.923205,10.752243],
        lat : 59.923205,
        lng : 10.752243,
        vertices :
        [
            { "y" : 10.752611, "x" : 59.923431},
            { "y" : 10.753963, "x" : 59.923764},
            { "y" : 10.754757, "x" : 59.92341},
            { "y" : 10.755765, "x" : 59.921538},
            { "y" : 10.755723, "x" : 59.921087},
            { "y" : 10.751538, "x" : 59.921226},
            { "y" : 10.750873, "x" : 59.921463},
            { "y" : 10.750573, "x" : 59.922313},
            { "y" : 10.750809, "x" : 59.92313},
            { "y" : 10.750852, "x" : 59.923571},
            { "y" : 10.752611, "x" : 59.923431}
        ]
    },
    
    {
        title : "MYREN (sone)",
        id : "zone-12",
        description: "Ved dette flate partiet av Akerselva mellom de store Bjølsen- og Vøyenfossene var det navnet til tross ikke sagbruk naturen la best til rette for. Navnet Sagene har da også kommet senere, og knytter seg mest til sagene som lå ved Vøyenfossen på nedsiden i elva og Bjølsenfossen på oversiden av elva. En hel liten landsby med over 20 hus for sagbruksarbeidere var her på 1600-tallet, og disse ble i folketellingene omtalt som 'Saugerne'. Ellers var det tykke lag av god leire som sørget for at teglsteinsproduksjon preget stedet. Norges første moderne papirfabrikk Bentse Brug ble lagt øverst på området fra 1695. Men det var Myrens Mekaniske Verksted som skulle legge hele Myren under seg fra en sped start i 1851. Helt til i 1987 var det høyt spesialisert tungindustri i de store hallene her på Myren. Vi kan lese årstall murt i den røde teglsteinen på de store fabrikkhallene som vitner om gode tider på 1940-50 og 60-tallet. I dag finner vi den mest karikerte gjenbruken av store industribygg: Innendørs klatrevegg og lekeland for barn, Kontorer og studioer for NRK, og filmstudioer hvor Norges største og lengstlevende TV-dramaserie blir filmet: Hotell Cæcar. I tillegg er det nye kontorbygg, og et av Norges beste lydstudioer: Rainbow. Mannen som importerte Tivoliradioen til Norge har sitt eget HiFi-museum her.",
        src: ["http://www.industrimuseum.no/filearchive/036_Myren_mellom_Sagene.mp3"],
        url: "http://www.industrimuseum.no/29myrenintro_tekst",
        zoneimg: "img/sone-12.png",
        img: "http://www.industrimuseum.no/imagearchive/ingressbilde_Akerselva2012_DagAndreassen-056.jpg",
        zonepoint: [59.936096,10.75814],
        lat : 59.936096,
        lng : 10.75814,
        vertices :
        [
            {"y" : 10.760443, "x" : 59.937333},
            {"y" : 10.761688, "x" : 59.936215},
            {"y" : 10.760143, "x" : 59.935645},
            {"y" : 10.760357, "x" : 59.934882},
            {"y" : 10.759735, "x" : 59.933258},
            {"y" : 10.759263, "x" : 59.932624},
            {"y" : 10.757246, "x" : 59.933248},
            {"y" : 10.756023, "x" : 59.934161},
            {"y" : 10.755594, "x" : 59.935086},
            {"y" : 10.756130, "x" : 59.937526},
            {"y" : 10.760443, "x" : 59.937333}
        ]
    }
];


// Global variables
var debug_flag = false;
var alert_debug_flag = false;
var loading_gif_old = '<img src="img/loading-animation.png">';
var loading_gif = '<img src="img/adi-loader.gif">';
var open_url_gif = '<img src="img/new_window_icon.gif">';
var myGlobalMap;
var debugBrowser = true;
var liveData = false;
var display = {
    
    /*************************************************************************
        Method to calculate distance between two points. Usually one of the 
        points is the users current position, with the other being some point 
        of interest.
    **************************************************************************/
    calculateDistance : function calculateDistance ( lat1, lon1, lat2, lon2 ) {
    	var R = 6371; // km Earth radius
    	var dLat = (lat2 - lat1).toRad();
    	var dLon = (lon2 - lon1).toRad(); 
    	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
        Math.sin(dLon / 2) * Math.sin(dLon / 2); 
    	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); 
    	var d = R * c;
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
        
    	return d;
    },
    
    /*************************************************************************
      This is for generating a static map, with a path for the tracked route
    **************************************************************************/
    generateMappedRoute : function generateMappedRoute(nodes) {
        
    	var path = "http://maps.googleapis.com/maps/api/staticmap?path=color:0x0000ff|weight:3|";
    	for (i=0;i<nodes.length;i++) {
            path += nodes[i]['lat'].toString() + "," + nodes[i]['lng'].toString() + "|";
    	}
    	
    	var mymap = path.substring(0,path.lastIndexOf("|")-1),
        htmlstruct = '<img src="' + mymap + '&amp;size=400x400&amp;sensor=false">';
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
        return htmlstruct;
    }, 
    
    /*************************************************************************
       Creating a static Google map with a set of the closest POIs marked
    **************************************************************************/
    generateMarkersMap : function generateMarkersMap(zoom, nodes) {
        
        if(!zoom){zoom=15}
        var centerednode = myLocation.lastKnownPosition; //This will break if not controlled better
        var clng = centerednode[0].toString();
        var clat = centerednode[1].toString();
        var polyline = '';

        if (debugBrowser){
            zoom = 15;
        }
        
        var map_start = 'http://maps.googleapis.com/maps/api/staticmap?center=' + clng + ','+ clat + '&zoom=' + zoom + '&size='+ myLocation.mapsize + 'x' + myLocation.mapsize;
        var cpos = '|' + clng + ','+ clat;
        var you_are_here = '&markers=icon:http://www.google.com/mapfiles/ms/micons/man.png'  + cpos.toString();
        marker_start = '&markers=color:blue|';
        marker_visited = '&markers=color:yellow|';
        markers = '';
        
        for (i=0;i<nodes.length;i++) {
            mark = display.numberLetterMatch(i);
            
            if(nodes[i]['visited']) {
                markers +=  marker_visited + "label:" + mark + "|" + "size:mid|" + nodes[i]['lat'].toString() + "," + nodes[i]['lng'].toString() + "|";
            }else {
                markers +=  marker_start  + "label:" + mark + "|" + "size:mid|" + nodes[i]['lat'].toString() + "," + nodes[i]['lng'].toString() + "|";
            }
        }        
        
        //your position:
        markers += you_are_here;
        map = map_start + markers;
        currentZone = myLocation.pollCurrentZone(centerednode);
        
        var vertices = currentZone.vertices;
        
        if (vertices != undefined){
            polyline = '&amp;path=';
        
        for (i=0;i<vertices.length;i++) {
            polyline += vertices[i].x + "," + vertices[i].y;
            if(i<vertices.length-1){
                    polyline += '|';
                }
            }
        }
        
        htmlstruct = '<img src="' + map + polyline + '&amp;sensor=false">';
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
        
        return htmlstruct;
    },
        
    /*************************************************************************
            Redraw the map when needed
    **************************************************************************/
    
    updateMap : function updateMap() {
        //if we have more than 15 markers, the map may not generate because of google limitations.
        var slicelimit = (myLocation.inProximity<15) ? myLocation.inProximity:15;
        var newmappedroute = myLocation.closestnodes.slice(0,slicelimit);
        var zoom = $("#mapzoomlevel").val();
        
        staticmapimage = display.generateMarkersMap(zoom, newmappedroute);
        $("#markermap").html(staticmapimage);   
        var mark = '';
        var htmlstruct = ''
        var nodelist =  '';
        var tmp;
        for (i=0;i<newmappedroute.length;i++){
            mark = display.numberLetterMatch(i);
            tmp = '<div class="mapnode" externallink="' + newmappedroute[i]["externallink"] + '" >' +'[' + mark + '] ' +   newmappedroute[i]["title"] + open_url_gif + '</div>';
            nodelist += tmp;
        }                  
        
        htmlstruct += nodelist;
        
        $("#proximity").html(htmlstruct);
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
    },
    
    /*************************************************************************
         Utility function - markers cannot have more than 1 character on them
    **************************************************************************/
        
    numberLetterMatch : function numberLetterMatch(index) {
        var str = "a b c d e f g h i j k l m n o p q r s t u v w x y z æ å ø"; // A B C D E F G H I J K L M N O P Q R S T U V W X Y Z Æ Ø Å";
        var letterarray = str.split(" ");
            
        if (index>=letterarray.length){
            return "?";
        }else{
            return letterarray[index].toUpperCase();
        }
    }

};

/*************************************************************************
    Utility function - enables a call stack
**************************************************************************/

function traceFunctions(ownName) {
    ownName = ownName.substr('function '.length);        // trim off "function "
    ownName = ownName.substr(0, ownName.indexOf('('));        // trim off everything after the function name
    console.log("Trace function: " + ownName);
}

function toggleLoader(element, className, addOrRemove) {
    element.toggleClass(className, addOrRemove);
}

var myLocation = {
    
    'route' : []  ,
    'closestnodes' : []  ,
    'inProximity' : 6  ,
    'updatecounter' : 0  ,
    'routeCounter' : 0  ,
    'wpid' : false  ,
    'foundFirstPosition' : false   ,
    'minAccuracy' : 100  ,
    'limit' : 0.05  ,
    'mapsize' : 400  ,
    'mapinterval' : 60  ,
    'keyDate' : null   ,
    'polling' : false  ,
    'lastKnownPosition': null  ,
    'currentZone': false,
    'poi': [], // points of interest
    'lastPoi': [],
    'zones': [], //can contain points of interest. {title : sagene, poi : [title : myrens]}
    'startTime' : null  ,
    'updateTimes' : []  ,
    'firstMapUpdate' : true  ,
    'pollCount' : 0,
    
    'init' : function init () {

        if(debugBrowser){
            myLocation.mapsize = 2400;
        }


        if (!myLocation.wpid){
            myLocation.startTime = myLocation.getTimeOfDay;
            $("#startTime").html(myLocation.startTime);
            
            if(liveData){
                myLocation.poi = myLocation.getData();  //Use for live data reading, zopedev or industrimuseum, due in next version.
            }else{
                myLocation.poi = myLocation.loadData(foundationData);  //Inline data
            }
            myLocation.zones = zones;           
            myLocation.watch();
        }
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
    },
        
    'watch' : function watch(){
        $("#closestnodes").html("Bestemmer posisjon.. ");
        loader();
        if (debugBrowser){
            myLocation.wpid = navigator.geolocation.watchPosition(myLocation.watchSuccess,
                                                              myLocation.watchSuccess,
                                                              {enableHighAccuracy:true,
                                                              maximumAge:30000, 
                                                              timeout:5000}
                                                              );
        }else{
            myLocation.wpid = navigator.geolocation.watchPosition(myLocation.watchSuccess,
                                                                  myLocation.watchError,
                                                                  {enableHighAccuracy:true,
                                                                  maximumAge:30000,
                                                                  timeout:27000}
                                                                  );
        }
        
        
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
    },
    
    
    'getTimeOfDay' : function getTimeOfDay() {
        var a_p = "";
        var d = new Date();
        var curr_hour = d.getHours();
        
        if (curr_hour < 12){
            a_p = "AM";
        }
        else{
            a_p = "PM";
        }
        
        if (curr_hour == 0){
            curr_hour = 12;
        }
        
        if (curr_hour > 12){
            curr_hour = curr_hour - 12;
        }
        
        var curr_min = d.getMinutes();
        
        curr_min = curr_min + "";
        
        if (curr_min.length == 1){
            curr_min = "0" + curr_min;
        }
        
        var curr_sec = d.getSeconds();
        if (curr_sec.length == 1){
            curr_sec = "0" + curr_sec;
        }
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
        return curr_hour + ":" + curr_min + "." + curr_sec + " " + a_p;
    },     
    
    /*****************************************************************************************
    Code to calculate compass direction (in degrees) from one position to another
      typically you want to display an arrow pointing to an object from your current position 
    ******************************************************************************************/
    'getBearing' : function getBearing(lat1,lon1,lat2,lon2){
        if (typeof(Number.prototype.toRad) === "undefined") {
            Number.prototype.toRad = function() {
                return this * Math.PI / 180;
            }
        }
        
        if (typeof(Number.prototype.toDeg) === "undefined") {
            Number.prototype.toDeg = function() {
                return this * 180 / Math.PI;
            }
        }
        
        var dLon = (lon2-lon1).toRad();
        lat1 = lat1.toRad();
        lat2 = lat2.toRad();
        
        var y = Math.sin(dLon) * Math.cos(lat2);
        var x = Math.cos(lat1)*Math.sin(lat2) -
        Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
        var brng = Math.atan2(y, x).toDeg();
        
        if (brng<0){
            return (180+(180+brng));
        }
        else{
            return brng;
        }
    },
    /*****************************************************************************************
     If triggered, we have a valid position, which can be used to calculate distances and if
     inside a zone.
     ******************************************************************************************/
    'watchSuccess' : function watchSuccess(position) {

	//if timeout spoof location, for debugging purposes, need to change to use watchError when in browser.
    //$("#adi-loader").css("display","none");
    //$("#adi-loader").html("");
    //unloader();
    //for testing purposes, simulate a position inside or outside a zone
	if (debugBrowser && (position.code == 3 || position.code == 1)){
        var myindex = 9;
        
        // 0: NYDALEN
        // 1: KJELSÅS
        // 2: SAGENE/VØYEN
        // 3: Kjelsås Mustad delsone
        // 4: LILLEBORG
        // 5: BJØLSEN
        // 6: ELVEBAKKEN
        // 7: FJERDINGEN
        // 8: VATERLAND
        // 9: BJØRVIKA
        // 10: ØVRE FOSS
        // 11: NEDRE FOSS
        // 12: MYREN
        
        // nodes
        for (i=0;i<foundationData.length;i++){
            coord = foundationData[i].coords.split(",");
            lng = parseFloat(coord[1]);
            lat = parseFloat(coord[0]);
        }
        
        var testPoint = zones[myindex].zonepoint;
        var testlat = 59.935344;
        var testlng = 10.756963;
        testlat = testPoint[0];
        testlng = testPoint[1];
      
        testlat = 59.938246;  //outside any zone
        testlng = 10.733886;  //outside any zone
        
        position.coords = {"latitude" : testlat, "longitude" : testlng, "accuracy" : 50}
	}
	
        //Keep track of where the user is
        myLocation.lastKnownPosition = [position.coords.latitude,position.coords.longitude];
        
        // 1. Check if we entered a new zone.
        // 2. Play soundfile if not played before
        // 3. Check if we hit a POI
        // 4. Play soundfile for POI. (Check flag, play all or first file)
        
        if (position.coords.accuracy <= myLocation.minAccuracy) {
            
            if (!mediaPlayer.lock){
                node = false;
                myLocation.currentZone = myLocation.pollCurrentZone(position);
                
                if (myLocation.currentZone && !myLocation.currentZone["visited"]){
                    //play zone intro
                    node = myLocation.currentZone;
                    
                    //run this method to update listings. Should be made more efficient.
                    myLocation.pollNearestObject(position);
                }else{
                    node = myLocation.pollNearestObject(position);
                }
                
                if (node){
                   myLocation.playAudio(node);
                }
            }
        }
        
        //we have a position, now use it!
        //google.maps.event.addDomListener(window, 'load', initializeMap);

        //initiate map on first load
        if(myLocation.firstMapUpdate){
            display.updateMap(); //static map, commented out
            myLocation.firstMapUpdate = false;
        }
        
        //reset frontpage on first posistional hit.
        if(!myLocation.foundFirstPosition){
            $("#adi-loader").html("");
            myLocation.foundFirstPosition = true;
        }        
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}

    },
    
    'watchError' : function (error) {
        //watch error
        console.log("WATCH ERROR " + error);
	},
    
    'getTimeStamp' : function () {
        // function if we want to format it later
        return new Date();
    },
    
    'sortNodesByDistance' : function (route) {
        myLocation.closestnodes = route;
        myLocation.closestnodes = myLocation.closestnodes.sort(function(a, b){
                                                               return a.distance-b.distance;
                                                               });
	
    },
    
    'loadData' : function loadData (foundationData, fasttrack){
	//load and parse the source into the app.

	/*note:
	  - adding fasttrack param, this only loads the first audiofile if multiple
	  */

        var localurl,
        description = '',
        title = '',
        externallink,
        hasNoImg,
        mapmarker,
	route = [],
	fasttrack = fasttrack || false;
        
        for (i=0;i<foundationData.length;i++) {
            
            item = foundationData[i];
            //get coords
            coord = item['coords'].split(",");
            lng = parseFloat(coord[1]);
            lat = parseFloat(coord[0]);
            audio = item['audio'];
            urls = [];
            streaming_urls = [];
            
            // This snippet is for changing the code to locally stored fils. Let's try with streaming instead. Should be extended to downloading all files.
	    if (fasttrack) {
            if (audio.length > 0){
		    streaming_urls.push(audio[0]);
            }
        
	    } else{
		$.each(audio, function(i, val){
                    streaming_urls.push(val);
                    localurl = val.split("/").reverse().shift();
                    localurl = 'audio/' + localurl;
                    urls.push(localurl);
                });
	    }
            
            imgobj = item['img'];
            imgurl = ''
            
            hasNoImg = isEmpty(imgobj);
            if (!hasNoImg){
              imgurl = imgobj['url'];
            }
            
            description = item['description'];
            title  = item['title'];
            externallink = item['url'];
            
            //create a consistent marker for det map. Map markers can only display one character, but there will never be more than 15 visible, so the alphabet is enough.
            mapmarker = display.numberLetterMatch(i);
            
            //generate id, using last part of url as id.
	    splitter = externallink.split("/")
	    currentid = "node-" + splitter.reverse()[0];

            route.push({'lat' : lat, 
                       'lng' : lng, 
                       'id' : currentid,
                       'time' : 'none',
                       'visited' : false,
                       'usertriggered' : false,
                       'visitingtime' : null, 
                       'src' : streaming_urls,   // urls, //audio[0],
                       'title' : title, // item['title'],
                       'distance' : 100000000,
                       'desc' : description, // item['description'],
                       'imgurl' : imgurl,
                       'externallink' : externallink,
                       'mapmarker' : mapmarker
                       
                    });
        }
        
        //setup the preferences
        $("#nodeLimit").html(foundationData.length);
        $("#visiblenodes").attr("max",foundationData.length);
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}
        myLocation.poi = route;
        return route;
    },

    'getData' : function getData(){
        //load data from remote server. Not in use until next version.
        $.ajax({url: "http://zopedev.bouvet.no/portal/seksjoner/kulturminneruter/akerselvasangen_dybwad/reallyGetAdiData",
		type : "get",
		dataType : "json",
		crossDomain: true,
		success : function (data){myLocation.loadData(data)},
		error: function (z, i, y){console.log("error");}
               }
              );
    },

    'setPlayMode' : function setPlayMode(mode){
    //utility function, to use during testing
	if (mode == "short"){
	    myLocation.poi = myLocation.loadData(foundationData, true);  //Use for testing and fallback
	}else if (mode == "full"){
	    myLocation.poi = myLocation.loadData(foundationData);  //Use for testing and fallback

	}
        myLocation.updateNodeListing();
    },

    'updateNodeDistance' : function updateNodeDistance(){
        // we udpate the distance or the pois each time we receive a new position.
        for (i=0;i<myLocation.inProximity;i++) {

            node = myLocation.closestnodes[i],
            dist = node['distance'];
            
            dist_i_meter = dist *1000;
            output_distance = '';
            
            if (dist_i_meter > 1000){
                output_distance += '<b class="distance">' + dist  + ' km </b>';
            }else{
                output_distance += '<b class="distance">' + dist_i_meter  + ' meter </b>';
            }
            
            idea = "#" + node['id'];
            $(idea).find(".distance").html(output_distance);
        }
        
    },
    'updateNodeListing' : function updateNodeListing() {
        
        var htmlinfo = '',
        currentid = '',
        theme = '',
        largeZoneImg = '',
        zone_img = '',
        zone_id = '',
        article_img = '',
        //default_img = largeZoneImg =  "img/no_image.jpeg";
        default_img = largeZoneImg =  "img/default-no-img.png";
        //add currentzone to the head of the listing
	    zone = myLocation.currentZone;
        
        if(zone != false){ //If not false: means we are inside a zone just now. If false, the user is outside any defined zone.
            zone_img = zone['zoneimg'];
            article_img = zone['img'];
            zone_id = zone['id'];
            articleImg = '<img class="displayed imgframe" src="' + article_img + '" >';
            largeZoneImg = '<img class="displayed imgframe zoneImage" src="' + zone_img + '" >';
            htmlinfo += '<div ' + 'id="' + zone_id + '" ' +  'class="node-container" data-role="collapsible" data-inset="true">' + '<h3>' + '<div class="first"><img class="previewImage" src="' + zone_img + '" > </div>' + '<div class="last">' + zone['title'] +  '</div>' + '</h3>' + articleImg  + '<p>'  + zone['description'] + '</p>' + largeZoneImg +  '</div>';
        }
        //add closest poi's
        for (i=0;i<myLocation.inProximity;i++) {
            
            dist = myLocation.closestnodes[i]['distance'];
            dist_i_meter = dist *1000;
            output_distance = '';
            
            if (dist_i_meter > 1000){
                output_distance += '<span class="distance">' + dist + ' km </span>';
            }else{
                output_distance += '<span class="distance">' + "Distanse: " + '<b>' + dist_i_meter + '</b>' + ' meter </span>';
            }

            // if visited change node color to a dark scheme
            if (myLocation.closestnodes[i]['visited']){
                theme = ' data-theme="a"';
            }else{
                theme = '';
                
            }

            currenttitle = myLocation.closestnodes[i]['title'];
            myImg = '';
            previewImg = '';
            currentimg = myLocation.closestnodes[i]['imgurl'];
            currentdesc = myLocation.closestnodes[i]['desc'];
            currentid = myLocation.closestnodes[i]['id'];
            currentexternallink = myLocation.closestnodes[i]['externallink'];
            openexternally = '<div class="mapnode" externallink="' + currentexternallink + '" >' + currenttitle + open_url_gif + '</div>';
            
            
            if (currentimg != '') {
                myImg = '<img class="displayed imgframe" src="' + currentimg + '" >';
                previewImg = '<img class="previewImage" height="56" width="56" src="' + currentimg + '" >';
            } else {
                previewImg = '<img class="previewImage" src="' + default_img + '" >';
            }
            
            var tracks = myLocation.closestnodes[i]['src'];
            var trackCode = '';
            var trackhelptxt = '<div class="small-intro small-margin-bottom">Trykk på ikonet for direkteavspilling av lydspor</div>';
            if (tracks.length == 1){
                trackCode = trackhelptxt + '<div class="tracks"><span class="track intro"><img src="img/adi_icon_short.png" width="44"></span></div>';
            }else if (tracks.length == 2){
                trackCode = trackhelptxt + '<div class="tracks"><span class="track intro" id="intro"><img src="img/adi_icon_short.png" width="44"></span><span class="track ext" name="ext"><img src="img/adi_icon_long.png" width="66" ></span></div>';
            }
            
            /*htmlinfo += '<div ' + 'id="' + currentid + '" ' +  'class="node-container" data-role="collapsible" data-inset="true" ' + theme + ' >' + '<h3>' + '<div class="first">' +  previewImg + ' </div>' + '<div class="last">' + currenttitle +  '<div>' + output_distance + '</div>' + trackCode + '</div>' + '</h3>' + myImg + '<p>'  + currentdesc + '</p> ' +  openexternally + '</div>';*/
	

            htmlinfo += '<div ' + 'id="' + currentid + '" ' +  'class="node-container" data-role="collapsible" data-inset="true" ' + theme + ' >'
                + '<h3>' + '<div class="first">' +  previewImg + ' </div>' + '<div class="last">' + currenttitle +  '<div>' + output_distance + '</div></div>' + '</h3>'
                + trackCode
                + myImg + '<p>'  + currentdesc + '</p> ' +  openexternally + '</div>';
            


        }
        
        //redraw the whole list
        unloader();
        $("#closestnodes").html(htmlinfo);
        
        // to ensure the nodes are collapsed when rendered
        $('div[data-role=collapsible]').collapsible();

        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}

    },
    
    'pollCurrentZone' : function pollCurrentZone(position){
    
        var isArray = position instanceof Array; // that means we did not get a proper position object, but an array with two coordinates
        var pos;
        
        if (!isArray){
            pos = {'x' : position.coords.latitude, 'y' : position.coords.longitude};        
        }else{
            pos = {'x' : position[0], 'y' : position[1]};
        }
        
        currentZone = false;
        
        $.each(myLocation.zones, function(counterindex, zone){
               if (myLocation.pointInZone(zone['vertices'], pos['x'], pos['y'])){
                    currentZone = zone;
            }
        });
        
        if(!currentZone){
            $('#information-container').html("<h2> &nbsp; &nbsp; &nbsp; Du er ikke i noen ADi-sone akkurat nå.</h2>");
        }
        
        return currentZone;
    },
    
    'pointInZone' : function pointInPolygon2(polySides, x, y) {
        
        var ii, jj=polySides.length-1;
        var  oddNodes=false;
        
        for (ii=0; ii<polySides.length; ii++) {
            if ((polySides[ii].y< y && polySides[jj].y>=y
                 ||   polySides[jj].y< y && polySides[ii].y>=y)
                &&  (polySides[ii].x<=x || polySides[jj].x<=x)) {
                if (polySides[ii].x+(y-polySides[ii].y)/(polySides[jj].y-polySides[ii].y)*(polySides[jj].x-polySides[ii].x)<x) {
                    oddNodes=!oddNodes; }}
            jj=ii; }
        
        return oddNodes; 
    },
    
    'pollNearestObject' : function pollNearestObject(position) {
        myLocation.pollCount += 1;
        
        var currentlat = position.coords.latitude,
        currentlng = position.coords.longitude,
        lat, 
        lng,
        distance,
        distanceStr,
        bearing, 
        hits = [],
        
        nodes = myLocation.poi;
        myLocation.lastPoi = $.extend(true,[], myLocation.poi);
        
        tmplat = nodes[0]['lat'];
        tmplng  = nodes[0]['lng'];
        
        closestnodedistance = null;
        closestnode = null;
        closestnodeindex = null;

        for (i=0;i<nodes.length;i++){
            
            lat = nodes[i]['lat'];
            lng = nodes[i]['lng'];
            
            bearing = myLocation.getBearing(currentlat, currentlng, lat, lng); 
            nodes[i]['bearing'] = bearing;
            
            distance = display.calculateDistance(currentlat, currentlng, lat, lng)
	    distanceStr = new String(distance);
	    nodes[i]['distance'] = distanceStr.substr(0,5);
            if (distance < myLocation.limit  && !nodes[i]['visited']){
                if (closestnode == null || distance < closestnodedistance ) {
                    closestnodedistance = distance;
                    closestnode = nodes[i];
                    closestnodeindex = i;
                }
                
                if (!nodes[i]['visitingtime']) {
                    nodes[i]['visitingtime'] = myLocation.getTimeOfDay();
                }
                
                nodes[i]['time'] = myLocation.getTimeStamp();
		if (!debugBrowser){
		    navigator.notification.vibrate(1000);
		}
                hits.push(nodes[i]);
		break;
	    }
        }
        
        //sort the nodes by distance
        myLocation.sortNodesByDistance(myLocation.poi);

        //if order changed in the displayed number of nodes, do update
        doUpdate = false;
        for(var i = 0;i<myLocation.inProximity;i++){
            if (myLocation.lastPoi[i]['title'] != nodes[i]['title']){
                doUpdate = true;
                break;
            }
        }
        //if order changed update whole list, else just distances
        if (doUpdate){
            myLocation.updateNodeListing();
        }else{
            myLocation.updateNodeDistance();
        }
       
         //return if hit
        if (hits.length > 0){
            return closestnode;
        }
        
        if(debug_flag){var ownName = arguments.callee.toString();traceFunctions(ownName);}

    },
    
    playAudio : function playAudio(audioElement, override, tracknum){
        override = override || false;
        tracknum = tracknum || false;
        desc = audioElement['desc'] || audioElement['description'] || 'No info';
        audioElement['visited'] = true;
        
        mediaPlayer.audioPlayer(audioElement['src'],override, audioElement['title'], tracknum, desc);
    }
    
};

function loader() {
    $("#adi-loader").html(loading_gif);
}

function unloader(){
    $("#adi-loader").html("");
}

mediaPlayer = {
    lock: false,
    queue: [],
    currenttrack: '',
    currentZoneName: '',
    tracktitle: '',
    currentdescription: '',
    currentheight: 600,
    audioInternal:  $("#audioPlayer")[0],
    
    skipTrack: function (){
	mediaPlayer.audioInternal.pause();
	endTime = mediaPlayer.audioInternal.seekable.end("random");
    
	mediaPlayer.audioInternal.currentTime = endTime - 0.1;
    mediaPlayer.audioInternal.muted = true;
	mediaPlayer.audioInternal.play();
    
    mediaPlayer.audioInternal.oncanplay=testaudio();
	
    },
    
    audioPlayer: function (src, override, track, tracknum, cdesc) {

        mediaPlayer.tracktitle = track;
        mediaPlayer.currentdescription = cdesc;
        mediaPlayer.currentheight = cdesc.length/2;
        
        if(override) {
            mediaPlayer.audioInternal.pause();
            mediaPlayer.lock = false;
            mediaPlayer.queue = [];
        }
        
        if (!mediaPlayer.lock){
            mediaPlayer.lock = true;
	    //if tracknum to play is set, just push the given tracknum to the queue.
	    if (tracknum != false){
		mediaPlayer.queue.push(src[tracknum]);
	    }else{
            mediaPlayer.queue = src.slice();
	    }
            
            var shortened_track = track;
            if (track.length>40){
                shortened_track = track.substring(0,70) + "..."; // do not list to long string as title pushes down the expand image.
            }
            $('#player-info').html("<span> Nå spilles: " + shortened_track + "</span>");
            currentZoneName = "Du er i :" + myLocation.currentZone.title;  //test streng
            if(myLocation.currentZone.title==undefined){
                currentZoneName = "Du er ikke i noen ADi-sone akkurat nå";
            }
            
            $('#information-container').html("<h3> " + currentZoneName + "</h3>" + "<p>Nå spilles: &nbsp;" +  track + "</p>" + "<p>" + cdesc + "</p>");
            
            mediaPlayer.audioInternal.src = mediaPlayer.queue[0];
            mediaPlayer.audioInternal.muted = false;
            
            //console.log("muted :" + mediaPlayer.audioInternal.muted);
            mediaPlayer.audioInternal.play();
        }
        
        mediaPlayer.audioInternal.addEventListener("ended", function() {
        
            mediaPlayer.queue.shift();
            mediaPlayer.lock = false;
            if (mediaPlayer.queue.length > 0){
                mediaPlayer.audioPlayer(mediaPlayer.queue, false, mediaPlayer.tracktitle, mediaPlayer.currentdescription);
            } else {
                $('#player').html('');
            }
        });
    }
};


/**************************************************************************
 Check if image is available, if not, a default empty "No image" is used
 ***************************************************************************/
function isEmpty(ob){
    for(var i in ob){ return false;}
    return true;
}

/**************************************************************************
 Additional track or zone info, expanded when orange icon in player container pressed 
 ***************************************************************************/
function expandPlayer(event){
    
    $(this).toggleClass("expandContainer");
    
    if ($(this).hasClass("expandContainer")){
        var thisheight = mediaPlayer.currentheight + 500 + "px";
        $("#player-container").css("height",thisheight);
        $("#information-container").css("display","block");
        
    }else{
        $("#player-container").css("height","60px");
        $("#information-container").css("display","none");
    }
    event.preventDefault();
    event.stopPropagation();
    return false;
}


/**************************************************************************
 Utility methods for opening url in external mobile browser, i.e. outside the application.
 ***************************************************************************/
function openBrowser(src){
    window.location.href = src;
    return false;
}

function retrieveSrc () {
    var currentsrc = $(this).attr("externallink");
    openBrowser(currentsrc);
    return false;
}

/**************************************************************************
 Direct play: aborts any currently played track and switches to the node pressed.
 ***************************************************************************/
function playNodeTrack (event) {
    
    event.preventDefault();
    
    var nodeId = $(this).parents(".node-container").attr('id'),
    trackId = $(this).attr("class");
    
    /*assuming we only have 1 or 2 tracks
      then the first track is played unless the track has the ext class set */
    tracknum = 0;
    if (trackId.indexOf("ext") != -1){
        tracknum = 1;
    }

    $.each(myLocation.poi, function(i, poi){
	if (poi.id === nodeId){
        myLocation.playAudio(poi, true, tracknum);
           }
    });
    
    return false;
}

function skipTrack(event){
    mediaPlayer.skipTrack();
    
}

function onDeviceReady() {
    //google.maps.event.addDomListener(window, 'load', initializeMap);
    
    $("#closestnodes").on("vclick", ".track", playNodeTrack);
    $("#player-container").on("vclick", ".expandplayer", expandPlayer);

    //skip button
    $("#player-container").on("vclick", "#skip", skipTrack);

    $('.ui-page').css('minHeight', screen.availHeight);
    
    $("#timeupdate .started").html("<b>First update: " + myLocation.getTimeStamp() + "</b>");
    myLocation.init();

    $("#updateMap").click($.debounce(750,function(event) {
        display.updateMap();
    }));

    //bind the settings panel to events.
    $("#fasttrack").bind("change", function(event, ui) {
        var onoff = $("#fasttrack").val();
        if (onoff == 'on'){
	    myLocation.setPlayMode("short");
        }else{
	    myLocation.setPlayMode("full");
        }
    });

    /**************************************************************************
        Binding to slider: the number of pois listed on the info page
     ***************************************************************************/
    $("#visiblenodes").bind( "change", function(event, ui) {
        myLocation.inProximity = $("#visiblenodes").val();
        myLocation.updateNodeListing();
    });

    /**************************************************************************
        Binding to slider: the accepted accuracy of the position. Low value 
        indicates a high accuracy. Typical default good value is 50.
     ***************************************************************************/
    $("#accuracy").bind( "change", function(event, ui) {
        myLocation.minAccuracy = $("#accuracy").val();
        myLocation.updateNodeListing();
    });
    
    /**************************************************************************
     Binding to slider: the distance in meters from the poi, before triggering audio.
     ***************************************************************************/
    $("#limit").bind( "change", function(event, ui) {
        floatlimit = $("#limit").val();
        floatlimit = parseFloat(floatlimit)/1000;
        myLocation.limit = floatlimit;     
        myLocation.updateNodeListing();
    });
    
    /**************************************************************************
     Binding to slider: the static map can be set to automatic updates, with interval
     in seconds.
     ***************************************************************************/
    $("#automap").bind("change", function(event, ui) {
        var onoff = $("#automap").val();
        myLocation.mapinterval = $("#mapinterval").val();
        if (onoff == 'on'){
            myGlobalMap = setInterval(function() {display.updateMap()}, myLocation.mapinterval*1000);
        }else{
            clearTimeout(myGlobalMap);
        }
    });
    
    /**************************************************************************
        The nodes external url as an attribute, 
        which is retrieved and used to open in the browser on click.
     ***************************************************************************/
    $("#proximity").delegate("div", "click", $.debounce( 500 /* milli seconds */, retrieveSrc) );
    $("#infoside").delegate("div.mapnode", "click", $.debounce( 500 /* milli seconds */, retrieveSrc) );
    
    };


    /**************************************************************************
    browser testing: if flag is true, we use simulated positions in browser
     ***************************************************************************/
    if (debugBrowser){
        $(document).ready(function() {
                          onDeviceReady();
    });
    } else {
        document.addEventListener("deviceready", onDeviceReady, false);
    }

